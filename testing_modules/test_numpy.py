import numpy as np
import time


def tic():
    # Set timer
    global clk
    clk = time.time()

def toc():
    # Print time elapsed in microseconds
    stop = (time.time() - clk)
    print("..." + str(np.floor(stop*1e6)) + "us")

# Test speed of numpy array actions
print("============ Test converting list of tuples into array")
# Make test list
lst_a = [( 1,2 ),( 3,4 ),( 5,6 ),( 7,8 ),( 9,10 )]
lst_b = [( 1,2 ),( 3,4 ),( 5,6 ),( 7,8 ),( 9,10 ),
         ( 1,2 ),( 3,4 ),( 5,6 ),( 7,8 ),( 9,10 ),
         ( 1,2 ),( 3,4 ),( 5,6 ),( 7,8 ),( 9,10 )]

print("list_a:\n", lst_a)
print("list_a:\n", lst_b)

print("------ array(list), direct conversion")
print("lst_a")
tic()
A = np.array(lst_a, dtype=np.float)
toc()

print("lst_b")
tic()
A = np.array(lst_b, dtype=np.float)
toc()

print("------ Allocate array, then fill with list contents")
print("lst_a")
tic()
A = np.empty((5,2), dtype=np.float)
A[:, :] = lst_a
toc()

print("lst_b")
tic()
A = np.empty((15,2), dtype=np.float)
A[:, :] = lst_b
toc()

# Allocating is faster, and difference is larger with larger arrays
