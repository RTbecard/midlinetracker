import numpy as np
import geometry as geo
import cv2 as cv

print("Make opencv greyscale array for testing")

# Init array
image = np.zeros([10, 20, 1], np.uint8)
# Paint values
h = 20
vals = [h*4  + 10, h*4 +  11, h*4 + 12,
        h*5  + 10, h*5 +  11, h*5 + 12,
        h*6  + 7,  h*6  + 8,  h*6  + 9, h*6 + 10, h*6 + 11, h*6 + 12, h*6 + 13,
        h*7  + 7,  h*7  + 8,  h*7  + 9, h*7 +  10,h*7 + 11, h*7 + 12,
        h*8  + 9,  h*8 +  10, h*8 + 11, h*8 + 12]

np.put(image, vals, 255)

cv.imshow("Test image", image)

print("---------- Draw horizontal line")
v = np.array([[1, 0]])
v = v.T
line = geo.line(geo.point(10, 5), v)

image_show = line.drawLine(image.copy(), 150)

cv.imshow("Horizontal Line", image_show)


print("------------ Draw vertical line")
v = np.array([[0, 1]])
v = v.T
line = geo.line(geo.point(10, 5), v)

image_show = line.drawLine(image.copy(), 100)

cv.imshow("Vertical Line", image_show)


print("---------- Draw diagonal line and its orthogonal compliment")
# low slope line
v = np.array([[2, 1]])
v = v.T
line1 = geo.line(geo.point(10, 5), v)

image_show = line1.drawLine(image.copy(), 150)

line2 = line1.orthogonal(line1.point)
image_show = line2.drawLine(image_show, 100)

cv.imshow("Diagonal Line and orth. comp.", image_show)


print("---------- Diagonal line and color intersecting detected pixels")

idx = line1.which(image, lambda x: x == 255)
image_show = image.copy()

print(idx)

for i in idx:
    image_show[i[1], i[0], :] = 150
cv.imshow("Diagonal Line Overlap", image_show)

idx = line2.which(image, lambda x: x == 255)
image_show = image.copy()

for i in idx:
    image_show[i[1], i[0]] = 150
cv.imshow("Diagonal Compliment Overlap", image_show)

# End program when keypress is captured on images
cv.waitKey(0)
