import cv2 as cv
import midlineEstimation as me
import matplotlib.pyplot as plt
from matplotlib import collections as clct
import numpy as np

# Load image and convert to grayscale
image = cv.imread("./test_bw.png")
gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
print("array Dims:", gray.shape)

# ======================= Horizontal line

# Init midline estimation object
midEst = me.MidlineEstimation()
midEst.bearing = 0
midEst.line_spacing = 10
midEst.low_pass = 0
midEst.final_points = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
midEst.setShape(gray.shape)
print("Border lines\n", midEst.lines_std_border)


# Get raw midline
midEst.rawLines()

# ------------ Get line segments to draw
draw = midEst.drawLines()
lc = clct.LineCollection(draw, colors="g", linewidths=1)
print(draw)

# ------------ Calculate raw midline points
raw = midEst.rawMidline(gray)

# ------------ Plot lines on bw image
# Draw backgorund image
fig, ax = plt.subplots()
plt.sca(ax)
plt.gray()  # Grayscale color pallete
image = ax.imshow(gray)
# Add lines
ax.add_collection(lc)
# Add midline estimates
raw_arr = np.array(raw)
plt.plot(raw_arr[:, 0], raw_arr[:, 1], marker='o', fillstyle="none",
         color="tab:red", linestyle="None")
# Add filtered estimates
midEst.setFilter(0.8, 3)
cln = midEst.runFilter(raw)
cln_arr = np.array(cln)

plt.plot(cln_arr[:, 0], cln_arr[:, 1], marker='x', fillstyle="none",
         color="tab:blue", linestyle="None")

# =================== Vertical line
midEst.bearing = np.pi/2

# Get raw midline
midEst.rawLines()

# ------------ Get line segments to draw
draw = midEst.drawLines()
lc = clct.LineCollection(draw, colors="g", linewidths=1)

# ------------ Calculate raw midline points
raw = midEst.rawMidline(gray)

# ------------ Plot lines on bw image
# Draw backgorund image
fig, ax = plt.subplots()
plt.sca(ax)
plt.gray()  # Grayscale color pallete
image = ax.imshow(gray)
# Add lines
ax.add_collection(lc)
# Add midline estimates
raw_arr = np.array(raw)
plt.plot(raw_arr[:, 0], raw_arr[:, 1], 'r.')


# =================== Diagonal line 1
midEst.bearing = 1
# Get raw midline
midEst.rawLines()
# ------------ Get line segments to draw
draw = midEst.drawLines()
lc = clct.LineCollection(draw, colors="g", linewidths=1)
print(draw)
# ------------ Calculate raw midline points
raw = midEst.rawMidline(gray)
# ------------ Plot lines on bw image
# Draw backgorund image
fig, ax = plt.subplots()
plt.sca(ax)
plt.gray()  # Grayscale color pallete
image = ax.imshow(gray)
# Add lines
ax.add_collection(lc)
# Add midline estimates
raw_arr = np.array(raw)
plt.plot(raw_arr[:, 0], raw_arr[:, 1], 'r.')


# =================== Diagonal line 2
midEst.bearing = -1
# Get raw midline
midEst.rawLines()
# ------------ Get line segments to draw
draw = midEst.drawLines()
lc = clct.LineCollection(draw, colors="g", linewidths=1)
# ------------ Calculate raw midline points
raw = midEst.rawMidline(gray)
# ------------ Plot lines on bw image
# Draw backgorund image
fig, ax = plt.subplots()
plt.sca(ax)
plt.gray()  # Grayscale color pallete
image = ax.imshow(gray)
# Add lines
ax.add_collection(lc)
# Add midline estimates
raw_arr = np.array(raw)
plt.plot(raw_arr[:, 0], raw_arr[:, 1], 'r.')


# =================== Diagonal line 3
midEst.bearing = 4
# Get raw midline
midEst.rawLines()
# ------------ Get line segments to draw
draw = midEst.drawLines()
lc = clct.LineCollection(draw, colors="g", linewidths=1)
# ------------ Calculate raw midline points
raw = midEst.rawMidline(gray)
# ------------ Plot lines on bw image
# Draw backgorund image
fig, ax = plt.subplots()
plt.sca(ax)
plt.gray()  # Grayscale color pallete
image = ax.imshow(gray)
# Add lines
ax.add_collection(lc)
# Add midline estimates
raw_arr = np.array(raw)
plt.plot(raw_arr[:, 0], raw_arr[:, 1], 'r.')


# =================== Diagonal line 4
midEst.bearing = -4

# Get raw midline
midEst.rawLines()
# ------------ Get line segments to draw
draw = midEst.drawLines()
lc = clct.LineCollection(draw, colors="g", linewidths=1)
# ------------ Calculate raw midline points
raw = midEst.rawMidline(gray)
# ------------ Plot lines on bw image
# Draw backgorund image
fig, ax = plt.subplots()
plt.sca(ax)
plt.gray()  # Grayscale color pallete
image = ax.imshow(gray)
# Add lines
ax.add_collection(lc)
# Add midline estimates
raw_arr = np.array(raw)
plt.plot(raw_arr[:, 0], raw_arr[:, 1], 'r.')


# Show plot
plt.show()
