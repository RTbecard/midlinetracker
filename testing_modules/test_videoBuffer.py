"""
Test video buffer module
"""
import videoBuffer
import time

# Initialize object
vb = videoBuffer.VideoBuffer("./test.avi", 3, 5, True)

# Get frames in range
print("####################################")
print("############ Get 9 frames from start")
vb.request_frame(1)
vb.request_frame(2)
time.sleep(0.5)
vb.request_frame(3)
vb.request_frame(4)
vb.request_frame(5)
vb.request_frame(6)
vb.request_frame(7)
vb.request_frame(8)
vb.request_frame(9)
time.sleep(0.1)

print("##############################")
print("############ Get last 3 frames")
vb.request_frame(8)
vb.request_frame(7)
vb.request_frame(6)
time.sleep(0.01)


print("##########################################")
print("############ Get last 3 frames from middle")
vb.request_frame(51)
vb.request_frame(52)
vb.request_frame(53)
time.sleep(0.01)

print("##############################################")
print("############ Get 3 previous frames from middle")
vb.request_frame(52)
vb.request_frame(51)
vb.request_frame(50)
time.sleep(0.01)

print("####################################################")
print("############ Let buffer fill 2 frames after requests")
vb.request_frame(1)
vb.request_frame(2)
vb.request_frame(3)
time.sleep(0.01)

print("#################################")
print("############ Frames 200, 250, 400")
vb.request_frame(200)
vb.request_frame(205)
vb.request_frame(440)
time.sleep(1)

vb.stop()
