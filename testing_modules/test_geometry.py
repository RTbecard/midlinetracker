import geometry as geo

box = geo.polygon([geo.point(1,2),
                   geo.point(2,2),
                   geo.point(2,1),
                   geo.point(1,1)])

poly1 = geo.polygon([geo.point(0,3),
                    geo.point(3,3),
                    geo.point(3,0)])

poly2 = geo.polygon([geo.point(0,3),
                    geo.point(3,3),
                    geo.point(3,0),
                    geo.point(0,0)])

line1 = geo.lineSegment(geo.point(0,1), geo.point(1,0))
line2 = geo.lineSegment(geo.point(0,1), geo.point(1,1))
line3 = geo.lineSegment(geo.point(0,0), geo.point(2,1))

print("#      line 1 intersects line 2:", geo.intersect_lineLine(line1, line2))
print("#      line 2 intersects line 3:", geo.intersect_lineLine(line2, line3))

print("#      box intersects poly 1:", geo.intersect_polyPoly(box, poly1))
print("#      box intersects poly 2:", geo.intersect_polyPoly(box, poly2))

print("#      line 1 intersects box:", geo.intersect_linePoly(line1, box))
print("#      line 2 intersects box:", geo.intersect_linePoly(line2, box))
