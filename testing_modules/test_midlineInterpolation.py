import midlineEstimation as me
import numpy as np

points = np.array([( 1,2 ),( 3,4 ),( 5,6 ),( 7,8 ),
                   ( 9,10 ),( 11,12 ),( 13,14 ),( 15,16 )])

print("points\n", points)

midline = me.interpolate(points, [0.1,0.5,0.9])

print("midline\n", midline)


