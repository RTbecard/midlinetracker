### Manual

- Add a flowchart for first time intended user base.  i.e. Batch processing from a standardised experimental setup. 
- Add `ffmpeg` tutorial for correcting misaligned videos
- Add schematics for ideal tracking setups.  Both stationary and transitory setups.