\documentclass[11pt]{book}

%%%%%%%%%%%%%%%%% Skeleton %%%%%%%%%%%%%%%%%%
% Font settings
\usepackage[T1]{fontenc}
\usepackage{libertine}
\renewcommand*\ttdefault{lmvtt}
\usepackage{inconsolata}

% Margins
% Set to MSword style margins
\usepackage[margin=1in]{geometry}

% Cursive Math Fonts
\usepackage{amsfonts}

% Section Formatting
\usepackage{titlesec}
\titleformat{\section}{\scshape\Large}{\thesection}{1em}{}[\titlerule]{}

% Caption formatting
\usepackage{caption}
\captionsetup[table]{skip=10pt}

% Equations
\usepackage{amsmath}
\mathchardef\mhyphen="2D % Define a "math hyphen"

% Images
\usepackage[table,dvipsnames]{xcolor}
\usepackage{graphicx}

% Floats
\usepackage{float}
% begin{figure}[H] to place figure in text at position

% Figure labelling (inside figures)
\usepackage{stackengine}
\def\stackalignment{l}
% \topinset{label}{image}{from top}{from left}

% Textwrapped figures
\usepackage{wrapfig}
% \begin{wrapfigure}[numberLines]{placement}[overhang]{width}

% Tables
\usepackage{booktabs}
\usepackage{tabularx}
% Fix rowcolors for tabularx
\newcounter{tblerows}
\expandafter\let\csname c@tblerows\endcsname\rownum

% List settings
\usepackage{enumitem}
\setlist{itemsep=0em,parsep=0em}
%Change to alphabetical listing
%\begin{enumerate}[label=(\Alph*)]

% SI unit characters (includiong non-italic)
\usepackage{siunitx}
\usepackage{eurosym}
% Define Euro symbol
\DeclareSIUnit{\pers}{pers}
\DeclareSIUnit{\EUR}{\text{\euro}}
\sisetup{
  per-mode = symbol,
  inter-unit-product = \ensuremath{{}\cdot{}},
  range-phrase=--,
  range-units=single
}
\DeclareSIUnit \amphour {Ah} %Define Amphour

% Code blocks
\usepackage{listings}
\definecolor{gitGrey}{rgb}{0.97,0.97,0.97}
\definecolor{gitRed}{rgb}{0.86,0.09,0.28}
\definecolor{gitGreen}{rgb}{0.07,0.60,.18}
\definecolor{ruleGrey}{rgb}{0.8,0.8,0.8}
\lstdefinestyle{github}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  backgroundcolor = \color{gitGrey},
  xleftmargin=0cm,
  extendedchars = true,
  showstringspaces=false,
  basicstyle=\ttfamily\small,
  keywordstyle=\ttfamily\small,
  commentstyle=\itshape\color{gitGreen},
  stringstyle=\color{gitRed},
  numbers=left,
  columns=fullflexible,
  tabsize=2}
  
%%%% Make R code enviroment
\lstnewenvironment{rcode}
{\lstset{style=github, language=R}}{}

\makeatletter
\DeclareCaptionFormat{form}{File\dotfill #3}
\captionsetup[lstlisting]{format=form,singlelinecheck=false,margin=0pt,
		font={tt},labelformat=empty,skip=0pt}
\newcommand{\rcodefile}[1]{
	\filename@parse{#1}
	\lstinputlisting[language=R, style=github,
	caption=\filename@base .\filename@ext]{#1}
}

% Hyper links
\usepackage[color links = true,
  all colors = blue]{hyperref}

% Set spacing in lists
\usepackage{enumitem}
\setenumerate{itemsep=3pt}
\setitemize{itemsep=3pt}

% Set default float placement
\makeatletter
\renewcommand*{\fps@figure}{p}
\renewcommand*{\fps@table}{tp}
\makeatother

% Hanging indents
\usepackage{hanging}

% Tikz
\usepackage{tikz}
\usetikzlibrary{shapes}
\usetikzlibrary{arrows.meta}
\usepackage{pgfplots}
\usetikzlibrary{decorations.pathmorphing}
%%%%%%%%%%%%%% Draft settings %%%%%%%%%%%%%
% Line numbering
\usepackage[switch, modulo]{lineno}
%\linenumbers

% Enable highlighting
\usepackage{soul}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{{\Huge\underline{Midline Tracker}}
\vspace{2em}\\{\Large Video Tracking of Midline Positions\\ 
for Swim Tunnel Experiments}}
\author{Designed and written by:\\James Campbell, Elsa Goerig, Ted Castro-Santos}

\newcommand{\button}[1] {\colorbox{Apricot}{\texttt{#1}}}
\newcommand{\parameter}[1] {\colorbox{GreenYellow}{\texttt{#1}}}
\newcommand{\image}[2]{
\begin{center}
	\vspace{1em}
	\noindent
	\includegraphics[width=#2]{#1}
	\vspace{1em}
\end{center}
}

\setcounter{secnumdepth}{3}  
\setcounter{tocdepth}{3}

\begin{document}
\frontmatter
\maketitle

\section*{Software License}

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <\url{https://www.gnu.org/licenses/}>.

\section*{Preface}

\textellipsis  text about the program, authors, why it was written, and how users can contribute.
\\
\textellipsis  Reference to resulting publication holding the first use of this software.

\tableofcontents

\mainmatter

\chapter{Quick Start}

Midline Tracker does a rather simple job, but there are a few concepts and parameters that we need to cover before you understand how it works.
Here, we'll provide a walk-through by analysing some videos that require us to use all the features of the software.
When you've finished this reading this chapter, you'll be able to start processing you're own videos.
The remainder of this manual deals with more technical explanations of the features and guidelines for pre- and post-processing which are not strictly necessary for understanding and running the program.


\section{File Opening and Navigation}

\subsection{Loading a Video}

Lest start with a single file.
When opening Midline Tracker, you'll be faced with a disabled user interface as no files are currently selected.
Navigate to the \button{Files} tab and press \button{Select Folder}.
Here you can select the root folder you're planning to work in.

\image{images/fileSelect}{\linewidth}

Once the folder is selected, the table will be populated by files found in that directory.
The \parameter{Filter} field can be used to specify that you only want to view video files.
Simply type the letters of the video file extension and the list table will filter out all non-matching files.

The columns \parameter{Prepared} and \parameter{Tracked} show if there are files related to previous runs of Midline Tracker Analysis for a given file.

To open a file, simply click on the row of the table that you want to open.
Midline Tracker should be able to open just about any video file type you throw at it.
If a video cannot be loaded, you'll see a notification in the bottom status bar.

After a video is selected, you'll notice that the user interface elements now become active.

\subsection{Video Navigation}

Select the \button{video} tab to see the video we've loaded.

\image{images/videoNavigation}{\linewidth}

While no other UI elements are in focus (simply click the image of the video frame to remove the focus from other elements), you can use the keyboard to move back and forth through the video with the following controls:

\begin{itemize}
	\item \button{z}/\button{x}: jump forward/backward by 1 frame.
	\item \button{a}/\button{s}: jump forward/backward by 10 frames.
	\item \button{q}/\button{w}: jump forward/backward by 100 frames.
	\item \button{1}/\button{2}: jump forward/backward by 1000 frames.
\end{itemize}

The \button{Play/Stop} button will result in a playback with a maximum frame rate determined by the field \parameter{fps}.

\section{Trackbox and Object Detection}

To start tracking our fish across the video frame, we need to:

\begin{enumerate}
	\item Set the trackbox dimensions and start position.
	\item Select the appropriate object detection method.
\end{enumerate}

Navigate to the first frame where the fish is fully visible in the tunnel and is not touching any boundaries.
Then press \button{Set Tracking Box} and draw a square around the animal by clicking the \textit{upper left} and then the \textit{lower right} bounds of the box.
When complete, you'll see a red box drawn over the animal.
It's a good idea to leave some margin space in this tracking box as only pixels within this box will be used for estimating the animals position.
If a portion of the fish's head or tail fall outside the box, this will be ignored in the midline detection.

\image{images/trackBox}{\linewidth}

Now you may see some dots plotted in the area.
These are the midline estimation points.
Since we haven't set up our detection parameters, we're not surprised to see these points are not particularity accurate at the moment.

Object detection operates by taking our colour image and converting it into binary a black and white image.
In this binary image, white pixels indicate a detected object while black is just the background we're hoping to ignore.
Press the \button{Tracking} tab to see a preview of the binary image used in object detection. 

\image{images/binaryImageRaw}{0.8\linewidth}

Now we see why those midline points were wildly inaccurate.
When you set your trackbox, this will implicitly set the \parameter{start frame} of your tracking analysis.
Only frames between the \parameter{start frame} and the \parameter{stop frame} will be used in the analysis.
The button \button{Mark Stopframe} will allow you to set the stopframe as the currently viewed frame, or alternatively you can manually set the stopframe with the field \parameter{Stop Frame}.

\subsection{Grayscale Threshold}
Let experiment by selecting the radio button \button{Grayscale threshold} objected detection method.
The parameter box \parameter{Grayscale Threshold} shows the available options.
This method operates by simply converting our video into grayscale and all resulting pixels which have a brightness value above or below a certain value are classified accordingly as object or background.
This is clear to see when comparing our resulting binary image to the above trackbox image.

\image{images/binaryImageGray}{0.8\linewidth}

You may have noticed that our \parameter{Threshold} parameter is set to 0.
This results in an \textbf{automatic threshold}.
To manually set a threshold, select any value between 1 and 255 (in other words, an unsigned 8 bit integer value).
If your fish appears as a bright object on a dark background, you can adjust for this by changing the parameter \parameter{Object on Background}.

In this case, we can see that the swim tunnel has a black bar which overlays the fish, resulting in false detections.
Hence, grayscale detection may not be the best approach for this video.

\subsection{Time Offset}

For videos which have a stationary background and the fish moves across the video through time, you can select \button{Time Offset}.
In this method the objected detection is achieved by comparing the brightness values of pixels currently in the trackbox to their values from \textit{n} frames before or after the current frame.

For this method to work you have to select an appropriate \parameter{Reference Frame Offset} value.
As a guideline, imagine the trackbox is in a fixed position.
Then try to measure how many frames it would take for your fish to start entering one side of the trackbox and fully exit through the other.
This is a good starting value to choose for this parameter.

\image{images/binaryImageTime}{0.8\linewidth}

The image above shows time offset tracking with a correct \parameter{Reference Frame Offset} while the image below shows tracking error resulting from too small a value.
In the lower image, we can see false object detection on the left side of the image.
Here, \parameter{Reference Frame Offset} frames after the current frame, the fish's tail is still inside the tracking box, resulting in an incorrect background image for object detection.

\image{images/timeOffsetWrong.jpeg}{0.8\linewidth}

For the \parameter{Threshold}, the automated value tends to be a bit lower than necessary.
You can usually get quite good results by choosing a high value (100 is a good starting point).
Here, the threshold indicates the change in brightness for a given pixel in the current frame, as compared to its value \textit{n} frames ago.
The background pixels should all be near 0, while the detected object will have a large range of values.

\subsection{Loop Tracking}

When testing out different object detection parameters, you can try pressing the \button{Loop Tracking} button.
This will start the playback from the starting frame.  
When the trackbox reaches the stopframe or the end of the video, the playback will loop back to the start frame and continue playing.
This provides a continuous playback loop where the trackbox is always active and is a useful tool for fine tuning your threshold values.

If you change tracking parameters during \button{Loop Tracking} mode, the video will show live updates to the tracking results.

\subsection{Clean Binary Image}

After applying the appropriate tracking method, we're often left with a messy binary image like below.
In the binary tracking image, \colorbox{lightgray}{gray} pixels indicate object detection  (i.e. pixels which pass your object detection threshold).
By default, Midline Tracker will take the largest group of detected pixels and mark that as the detected object, indicated by \colorbox{lightgray!30}{white} pixels.

\image{images/cleanBinary1.jpeg}{0.8\linewidth}

In the option box \parameter{Clean Binary Image}, we've provided some \href{https://docs.opencv.org/4.1.2/d9/d61/tutorial_py_morphological_ops.html}{simple morphological operations} that you can apply to the image to clean it up.
\button{Close} will cause closely spaced patches of white pixels to merge into a single patch, while \button{Open} will result in small patches of pixels disappearing; or single, continuous patches connected by thin lines to be broken into multiple isolated patches.
Below we've applied the \button{Close} operation which fills in patches of black pixels in the main body of the detected object.

The strength (i.e.\ spatial scale of) of the morphological operations can be increased by setting a higher \parameter{Diameter} or \parameter{Iterations} value for each type of operation.
A large diameter with low iterations is more computationally efficient than increasing the iterations and lowering the diameter, but the latter option tends to result in cleaner shapes for a similar effect size.

\image{images/cleanBinary2.jpeg}{0.8\linewidth}

The detected object is now a smoother patch of white pixels.
However, note that the head and the tail of the fish are not marked as detected.
When your fish is marked by multiple large patches of detected pixels, you can use the \parameter{Min. Blob Area} to mark all groups of contiguous pixels as the detected object.

Setting this parameter to 100 allowed us to detect the fish's tail in the image below.

\image{images/cleanBinary3.jpeg}{0.8\linewidth}

Now, the some of the fish is still undetected in this image as the patch of pixels marking the head has an area smaller than our \parameter{Min. Blob Area} threshold.
We're choosing to leave this as undetected.
By setting \parameter{Min. Blob Threshold} too low, we increase the risk of incorrectly marking small patches of detection noise as our fish.
Broadly speaking, a few frames of missing head or tail sections of the fish will not cause problems in the final data analysis.
These frames can be easily identified in post processing by measuring the total midline length of the fish on a frame-by-frame basis.

\section{Midline Estimation}

Okay, now that we've tuned our object detection so that the patches of white pixels in the binary image reasonably resemble our fish's location and shape, we can start digitising the midline.
In the images above, you've likely noticed that the binary image has a series of vertical green lines plotted over it.
These lines are used to generate the \textit{raw midline estimates} via the following algorithm applied to each line:

\begin{enumerate}
	\item Identify all pixels in the tracking frame which overlap with a given estimation line.
	\item Identify which of these pixels are marked as detected.
	\item Calculate the average position of all detected pixels touching the line.
	\item Project this averaged position back onto the line.
\end{enumerate}

The results of this algorithm are presented as \textcolor{RedOrange}{red crosses} in the binary image.

\subsection{Target Bearing}

In cases where the fish is not swimming strictly horizontally across the frame, you can adjust the orientation of the raw midline detections guides by pressing \button{Set Bearing} in the \button{Video} tab and clicking the starting and end positions of the fish in the frame preview.
Alternatively, you can also manually enter a parameter in the \parameter{Target Bearing} field, followed by pressing enter.
Note that angles are provided in radians, where 0 radians represents a bearing of due east.
Below is an example of the resulting midline guides where we've set the bearing to 0.1 radians.

\image{images/targetBearing.jpeg}{0.8\linewidth}

\subsection{Line Spacing}

In addition to changing the orientation of the raw midline guides, we can also change the spacing.
The estimation of these midline points can be a bit computationally expensive, so increasing the \parameter{Line Spacing} parameter, can result in faster tracking. 

\image{images/lineSpacing.jpeg}{0.8\linewidth}

\subsection{Low-pass Filter and Interpolated Trackpoints}


Finally, you're probably wondering what those green and blue point markers are on the binary image.
These indicate the \textcolor{green!60!black}{low-pass filtered} and \textcolor{blue!80!black}{interpolated} estimated midline points, respectively.

For those familiar with discreet signal processing, you may have noted that the shape of a fish's midline while swimming strongly resembles that of a superposition of a few sine waves.
Due to this similarity, we can utilize a low pass filter to clean out detection error noise in our midline position estimates.
In Midline Tracker, we calculate our final midline positions with the following algorithm:

\begin{enumerate}
	\item Raw midline points are arranged into a time series, where the x-axis of the series has a space of 1 unit between each point.
		The y-values of the time series are the distance of a raw midline point from the longitudinal guide line in the binary image.
		The sample rate of the time series is set as \SI{1}{\hertz}.
	\item A \href{https://en.wikipedia.org/wiki/Butterworth_filter}{Butterworth filter} is applied twice in each direction (forward and backward), resulting in 0-phase-shift filtering.
	\item The resulting filtered points are reprojected back onto the tracking box space.
	These are the \textcolor{green!60!black}{filtered midline positions}.
	\item The filtered points are then interpolated, resulting in a fixed number of midline point estimates per frame.
		These are the \textcolor{blue!80!black}{interpolated midline positions}, and this is the final midline position data that gets returned in the resulting \texttt{mttracker} file when the tracking completes.


\end{enumerate}

After an infinite impulse response (IIR) filter's coefficients are calculated, applying the filter to a signal is computationally cheap to implement.
Hence, the low pass filter stage of Midline Tracker's detection process causes little (if any) noticeable effect on the speed of tracking.

In the image below, you can see the vertical jitter of the \textcolor{RedOrange}{raw midline points} is cleanly removed in the \textcolor{green!60!black}{filtered midline points}.

\image{images/filter.jpeg}{0.6\linewidth}

When applying the low-pass filter, you can specify the \parameter{cut-off} frequency and the \parameter{order} (i.e.\ strength) of the filter.
Typically, the tail of the fish tends to be the most sensitive to \textit{over smoothing} when applying and aggressive filter.
Also note that the \parameter{Cut-off} frequency of the filter is conditional on your selected \parameter{Line Spacing} parameter.
For instance, doubling your \parameter{Line Spacing} will require you to divide your \parameter{Cut-off} frequency by two to maintain the same filter effect.

Finally, you can use the \parameter{\# of Track Points} parameter to specify how many evenly spaced, linearly \textcolor{blue!80!black}{interpolated points} along the midline you want to output for each frame in the resulting \texttt{mttracker} file.

\subsection{Saving Tracking Parameters}

Now that you've fine tuned your object and midline detection parameters, you can press \button{Save Setup} to save all this information to a \texttt{mtbox} file.
When you press \button{Reload Setup}, or reselect this video from the \parameter{File Select} table, the trackbox and object detection settings will be automatically loaded from this file.

\subsection{Saving Tracking Results}

After previewing your tracking settings with the \button{Loop Tracking} button, you're ready to execute the analysis.
Simply press \button{Track File} to run the tracker and save the results to a \texttt{mttracker} file.
The video will automatically jump to the start frame and begin playback until the stop frame is reached.

\section{Batch File Analysis}

Batch tracking is quite straight forward.
After you have a number of videos with prepared tracking settings (i.e. \texttt{mtbox} files), you'll notice \textcolor{green!60!black}{green check marks} beside the video file names in the table under the \button{Files} tab.
These videos are ready for batch tracking.

You can manually set each file's checkbox or press the \button{mark Prepared} button to automatically select all prepared files for batch tracking.
When ready, simply press \button{Track Batch}.
Each marked file will be sequentially loaded and tracked.

Finally, if you check the box \button{Export Track Vids}, each tracked file will return a proofing video where the resulting midline positions in the \texttt{mttracker} file are painted on the original video.
We suggest using this when possible, as it serves as a simple validation of your tracking analysis in case you encounter suspicious data later on in your post-processing activities.

After tracking, you'll notice that the column \texttt{Tracked} in the \button{Files} table will have \textcolor{green!60!black}{green check marks} beside each tracked video.
The button \button{Mark Prepared and Untracked} is a convenient way to only mark those file for batch tracking which have been prepared and don't currently have any tracking output.

\image{images/batchTracking.jpeg}{\linewidth}

\chapter{Features of Midline Tracker}

\chapter{Suggestions for Pre-processing}

\chapter{Suggestions for Post-processing}

\end{document}
