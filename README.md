# Midline Tracker

**Midline Tracker** is an open source application written by biologists for the analysis of midline movements of fish in swim tunnel experiments.
This software is designed to be simple, transparent, and facilitate batch processing of multiple videos with minimal user input.
To get an idea of what **Midline Tracker** can do (and if it's suitable for your needs), we suggest reading over the _quick start_ section of the user manual linked below.

![](./screenshot.jpeg)

**Midline Tracker** can be run from source (`midlinetracker.py`), or you can download one of our executable packages in the links below.
The packaged executables are standalone applications that don't require additional software to be installed.

## Downloads

- [User Manual](https://gitlab.com/RTbecard/midlinetracker/-/jobs/artifacts/master/raw/user_manual/user_manual.pdf?job=user_manual)

### Windows and Linux

The links below should provide the latest stable version of **Midline Tracker**.
The executable files are packaged in [7z archives](https://www.7-zip.org/) to reduce download sizes.
Simply decompress the downloaded archive and run the executable file found in the resulting folder (`midlinetracker.exe` on Windows).

- [Ubuntu 18.04](https://gitlab.com/RTbecard/midlinetracker/-/jobs/artifacts/master/raw/src/midlinetracker.7z?job=build-ubuntu)
- [Windows 10](https://gitlab.com/RTbecard/midlinetracker/-/jobs/artifacts/master/raw/src/midlinetracker.7z?job=build-windows)

## Building Locally

In case the above links don't work on your machine (such as for Windows 7 or Mac OS X users), we've provided some conveniance scripts for building the executable file on your local machine.
On all platforms, the script will:

1. Create an isolated python virtual environment.
2. Install all the required python modules within this enviornment.
3. Build the executable file.

Running these scripts should have no affect on your existing python installation.
On all platforms, you'll need to have a copy of [python 3](https://www.python.org/downloads/) installed on your system to run the script.

Below are the platform specific instructions for running the local build.

### Windows

1. Download and extract the contents of this repository anywhere on your machine.
2. Open the batch script located at `.\midlinetracker\src\package_locally_windows.bat` with a text editor.
3. Change the contents of line 8 to point to the installation directory of your python distribution.
For example, on our testing Windows 10 machine this was: `set python=C:\Users\ace38\AppData\Local\Programs\Python\Python37`.
4. Save the script and run it by either *a)* double clicking it or *b)* opening it from the cmd or powershell terminal.
We suggest using one of the terminals so you can see any error messages if the python path was specified incorrectly.
5. When the script is finished, the resulting exe file can be found in: `.\midlinetracker\src\dist\midlinetracker.exe`.
The folder `dist` can be distributed to other machines with the same version of Windows.

### Mac OS X

> This is on my to-do list

## To-Do list

A list of features currently in development.

- Support for MOG2 background subtraction algorithem.
- Add instructions for Mac OS X packaging.
- Write Developer section of manual.
- Clean up code, so it can be more easily modified by users (this was coded in great haste :p).

## Feature Requests 

The manual has a detailed *developer* section for those who are interested in expanding the functionality of the software.
For those lacking confidence in their python skills, we're open to adding features which may be broadly applicable to researchers working on biomechanics.  Feel free to [make an issue](https://gitlab.com/RTbecard/midlinetracker/issues) on this repository and tag it as a `feature-request`.
That being said, **we won't add features which are not specifically related to midline tracking**, such as correcting problems with videos which could be considered preprocessing.

Finally, pull requests are welcome...
Just be sure to tell us what you plan to do (and how) before making the request.

### Dependencies

**Midline Tracker** depends on the following python packages.
The version numbers indicate what's used in the packaging of executable files (i.e. the above download links).

- `PyQt==5.14.0`
- `numpy==1.17.1`
- `opencv-python==4.1.4.30`
- `scipy==1.4.1`
- `pyqtgraph==0.10.0`
- `dill==0.3.1.1`
- `tornado==6.0.3`

All executables provided in the download links were packaged using `PyInstaller==3.6` running in `python 3.7`.
Windows versions were packaged within a 64-bit `wine 4.14` environment hosted on a Ubuntu 18.04 docker image.

## Developer Version Downloads

The links below are not intended for typical users.
These point to the _in development_ latest versions of **Midline Tracker**.
These are expected to be unstable and are only included for testing purposes.

- [Ubuntu 18.04](https://gitlab.com/RTbecard/midlinetracker/-/jobs/artifacts/dev/raw/src/midlinetracker.7z?job=build-ubuntu)
- [Windows 10](https://gitlab.com/RTbecard/midlinetracker/-/jobs/artifacts/dev/raw/src/midlinetracker.7z?job=build-windows)

## News

2020-02-29: Fixed bug in Windows exe package.  No longer compiling with `-F`.  
2020-02-24: Windows and Ubuntu versions are sucesfully compiling in gitlab-runner.  
2020-02-19: Uploaded draft version of tracker and working on gitlab-runner setup.  
