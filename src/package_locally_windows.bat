echo "========================================================================"
echo "= Build python executable in a virtual environment                     ="
echo "=                                                                      ="
echo "= Make sure to set the path to your python 3 install directory on      ="
echo "= line 8 of this script                                                ="
echo "========================================================================"
:: Set python path
set python=C:\Users\ace38\AppData\Local\Programs\Python\Python37

echo off

echo Python Path: %python%\python.exe


:: Check if python path if correct 
%python%\python.exe --version 2>NUL
if errorlevel 1 goto error else goto install

:install
echo "========================================================================"
echo "========== Make virtual environemnt and install packages ==============="
echo on

:: Create build environment in current directory
%python%\python.exe -m venv env

:: Validate virtual env activation
echo "Location of current build enviornment: "
where python

.\env\Scripts\python.exe -m pip install ^
	PyInstaller==3.6 ^
	scipy==1.2.1 ^
	opencv-python==4.1.2.30 ^
	tornado==6.0.3 ^
	pyqtgraph==0.10.0 ^
	dill==0.3.1.1 ^
	numpy==1.17.1 ^
	PyQt5==5.14.0


echo off
echo "========================================================================"
echo "========== Running Pyinstaller ========================================="
echo on

.\env\Scripts\python.exe -m PyInstaller -y --clean --noupx ^
	--add-data mainwindow.ui;. ^
	--console ^
	--hidden-import codecs ^
	--hidden-import numpy.random.common ^
	--hidden-import numpy.random.bounded_integers ^
	--hidden-import numpy.random.entropy ^
	.\midlinetracker.py

echo off
echo "======================================================================="
echo "Build complete.  Program saved to: .\dist\midlinetracker.exe"


goto :EOF

:error
echo "Python not found at path."
echo "Please set the correct path to python3 installation at top of script."
