#-----------------------------------------------------------------------------
# Copyright 2020 James Adam Campbell
#-----------------------------------------------------------------------------
# This file is part of MidlineTracker.
#
# MidlineTracker is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MidlineTracker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MidlineTracker.  If not, see <https://www.gnu.org/licenses/>
#-----------------------------------------------------------------------------

import geometry as geo
import numpy as np
from scipy import signal as sig
# debugging
import time


def rotMat(theta):
    """
    Build a rotation matrix for lienar transforms.
    """
    #print("...midlineEstimation.rotMat...")
    R = np.array([[np.cos(theta), -np.sin(theta)],
                  [np.sin(theta), np.cos(theta)]])

    #print("Rot mat\n", R)

    return R

def interpolate(pts_arr, pos):
    """
    Interploate positions along midline points

    Parameters
    ----------
    pts_arr : numpy.array(shape=(:, 2), dtype=numpy.float)
        Array holding x and y positions of midline points.
    pos : list(float, float, float, ...)
        A list of floats (between 0 and 1) indicating the position along
        the midline to return in units of proportional length of the
        entire midline.
    """

    debug = False

    # Calculate displacement array
    disp = np.append(np.array([(0, 0)]), np.diff(pts_arr, axis=0), axis=0)
    if debug:
        print("disp\n", disp)

    # Calculate distance alone each axis
    distance_vec = np.cumsum(abs(disp), axis=0)
    if debug:
        print("distance_vec\n", distance_vec)

    # Calculate distance combined across axes
    distance_comb = np.sum(distance_vec**2, axis=1) ** 0.5
    if debug:
        print("distance_comb\n", distance_comb)

    dist_tot = distance_comb[distance_comb.shape[0] - 1]

    # ------ Interpolate each point
    def interp(p):
        #print("point: ", round(p, 2))
        # Target distance to interpolate
        dist_targ = dist_tot*p
        # Find last point less than target distance
        idx = np.max(np.nonzero(distance_comb <= dist_targ))
        #print("idx:", idx)

        # Calculate distance to travel after last point
        dist_remain = dist_targ - distance_comb[idx]
        #print("dist_remain:", dist_remain)

        if dist_remain == 0:
            return pts_arr[idx, :]
        else:
            # Calculate interp position
            lmbda = dist_remain / np.sum(disp[idx + 1, :]**2) ** 0.5
            #print("lmbda:", lmbda)
            return pts_arr[idx, :] + (disp[idx+1,:]*lmbda)


    out_arr = np.empty((len(pos), 2), dtype=np.float)
    #print("pos:", pos, "\nout_arr:", out_arr)
    out_arr[:, :] = list(map(interp, pos))

    return(out_arr)

class MidlineEstimation:

    debug = False

    def __init__(self):

        # ------------ Set by functions
        self.b_line = None
        # Shape of array to draw one
        self.shape = None
        self.lines_std_border = None
        # Filter coefs
        self.low_pass = 0
        self.order = 0
        self.a = None
        self.b = None
        self.cutoff = None

        # ------------ Set by attributes
        self.bearing = None
        self.line_spacing = None
        self.final_points = None

        # Lines used to generate
        self.lines = None

    def setFilter(self, low_pass, order):
        """
        Calculate butterworth filter coefficients
        """
        if self.debug:
            print("...midlineEstimation.setFilter...")

        self.cutoff = low_pass

        if order == 0:
            self.a = self.b = None
            self.order == 0
        else:
            # Sample rate of "signal" is 1
            [self.b, self.a] = sig.butter(order, low_pass/2)
            self.order = order

        if self.debug:
            print("............Filter a:", self.a, ", b:",
                  self.b, ", order:", self.order)

    def runFilter(self, points):
        if self.debug:
            clk = time.time()
            print("...midlineEstimation.runFilter...")

        # Skip of order is 0
        if self.order == 0:
            return np.array(points)

        # ------ Convert midpoint esitmations into waveform
        # Apply linear transformation (rotation) to make the points horizontal
        points_rot = list(map(lambda p: np.dot(rotMat(-self.bearing),
                                               np.array([[p[0]], [p[1]]])),
                              points))

        points_rot_arr = np.array(points_rot)
        #print("points_rot_arr\n", points_rot_arr)

        # Apply filter (both directions to correct phase offsets)
        try:
            signal_filt = sig.filtfilt(self.b, self.a,
                                       points_rot_arr[:, 1],
                                       axis=0,
                                       method="gust")
        except:
            print("......Not enough samples for filter... skipping")
            signal_filt = points_rot_arr[:, 1]

        points_rot_arr[:, 1] = signal_filt
        #print("points_rot_arr2\n", points_rot_arr)

        # Transform points back into trackbox space
        points_filtered = np.apply_along_axis(
            lambda p: np.dot(rotMat(self.bearing),
                             np.array([[p[0]], [p[1]]])), 1, points_rot_arr)
        points_filtered = np.squeeze(points_filtered)
        #print("points_filtered\n", points_filtered)

        if self.debug:
            print("......midlineEstimation.runFilter Timer: " +
                  str(np.floor((time.time()-clk)*1000)) + "ms")

        return(points_filtered)

    def setShape(self, shape):
        # Save share attributes
        self.shape = shape

        # Save border lines in standard form
        self.lines_std_border = []

        lines = [geo.line(geo.point(shape[1], shape[0]), geo.colVec(0, 1)),
                 geo.line(geo.point(shape[1], shape[0]), geo.colVec(1, 0)),
                 geo.line(geo.point(0, 0), geo.colVec(0, 1)),
                 geo.line(geo.point(0, 0), geo.colVec(1, 0))]

        self.lines_std_border = list(map(lambda x: x.standard(), lines))

    def ready(self):
        """
        Returns true if all required parameteres for midline estimation are set.
        """

        if self.debug:
            print("...midlineEstimation.ready...")
        print("......b_line:", self.b_line,
              "\n......shape:", self.shape ,
              "\n......lines_std_border:", self.lines_std_border,
              "\n......bearing:", self.bearing)

        fail = self.b_line is None or self.shape is None or \
           self.lines_std_border is None or self.bearing is None

        return not fail

    def rawLines(self):
        """
        Define a set of lines on which to estimate the midline positions.
        """

        if self.debug:
            print("...midlineEstimation.rawLines...")

        # Convert bearing to vector
        self.bearing_vector = geo.colVec(np.round(np.cos(self.bearing), 5),
                                         np.round(np.sin(self.bearing), 5))
        # Normalize vector to unit length
        eigVal = np.dot(self.bearing_vector.T, self.bearing_vector) ** 0.5

        self.bearing_vector = self.bearing_vector * eigVal
        self.line_vector = geo.colVec(np.round(-self.bearing_vector[1, 0], 5),
                                      np.round(self.bearing_vector[0, 0], 5))

        if self.debug:
            print("......bearing_vector:\n", self.line_vector)
            print("......line_vector:\n", self.line_vector)
            print("......Array shape (height, width):\n", self.shape)

        self.lines = []  # Init empty list

        # Find start and end of bearing line
        self.b_line = geo.line(geo.point(self.shape[1]/2, self.shape[0]/2),
                          self.bearing_vector)

        start_b, end_b = self.segment(self.b_line)
        # Convert to points
        start_b = geo.point(start_b[0], start_b[1])
        end_b = geo.point(end_b[0], end_b[1])

        #print("--- Bearing ---")
        #print(start_b, end_b)

        # Get num of lines to draw
        v = end_b.columnVector() - start_b.columnVector()
        v_len = np.dot(v.T, v) ** 0.5
        v_norm = v/v_len
        line_n = np.floor(v_len/self.line_spacing)

        # Define lines
        for i in range(0, int(np.floor(line_n))):
            p = geo.point(start_b.x + i * v_norm[0,0]*self.line_spacing,
                          start_b.y + i * v_norm[1,0]*self.line_spacing)
            self.lines = self.lines + [geo.line(p, self.line_vector)]

    def rawMidline(self, array):
        """
        Calculate the raw estimates of the midline position.
        """
        if self.debug:
            clk = time.time()
            print("...midlineEstimation.rawMidline...")

        if self.lines is None:
            self.rawLines()

        def getPoint(line):
            # Get pixels overlapping with line (gray value = 255)
            pixels = line.which(array)
            if pixels.shape[0] == 0:
                return(None)
            # Calc mean coordinate
            else:
                p = tuple(np.mean(pixels, axis=0))
                # ------ Project point onto line
                # Get orthogonal line
                #print("pixels\n", pixels)
                #print("p\n", p)
                line_ort = line.orthogonal(geo.point(p[1], p[0]))
                # Organize line and orth compliment in matrix
                std1 = line.standard()
                std2 = line_ort.standard()
                A = np.array([std1[0:2],
                              std2[0:2]])
                c = np.array([[std1[2]], [std2[2]]])
                #print("A:\n", A, "c:\n", c)
                # Solve system of eq for col 3
                v = np.dot(np.linalg.inv(A), c)
                return tuple(v[:, 0])

        means = list(map(lambda x: getPoint(x), self.lines))
        #print("means", means)
        # Remove empty values

        if self.debug:
             print("...midlineEstimation.rawMidline Timer: " +
                   str(np.floor((time.time()-clk)*1000)) + "ms")

        return list(filter(lambda x: x is not None, means))

    def drawLines(self):
        """
        Return a list of start and end positions of each line to be drawn in
        the image array.
        """
        print("...midlineEstimation.drawLines...")
        return(list(map(lambda x: self.segment(x), self.lines)))

    def segment(self, line):
        """
        Convert the line object into a line segment described by two points
        that fits within the tracking image.

        Calculate line intersect with tracking box.
        """
        #print("...midlineEstimation.segment...")
        # ----------- Calculate intersects for each border wall
        def intersect(l1, l2):
            # Returns a tuple of (x, y)
            # Lines must be tuples of standard form coefficients
            # Av = b
            A = np.array([l1[0:2], l2[0:2]])
            b = np.array([[l1[2]], [l2[2]]])

            v = np.dot(np.linalg.inv(A), b)

            return(tuple(v[:, 0]))

        # ------ Vetical line
        if line.vector[0] == 0:
            start = (line.point[0], 0)
            end = (line.point[0], self.shape[0])
        # ------ Horizontal lines
        elif line.vector[1] == 0:
            start = (0, line.point[1])
            end = (self.shape[1], line.point[1])
        # ------ Diagonal lines
        else:
            # Get border intercepts
            ixs = map(lambda x: intersect(x, line.standard()),
                      self.lines_std_border)
            # Remove out of bounds borders
            ixs = list(filter(lambda p: p[0] >= 0 and p[0] <= self.shape[1] and
                              p[1] >= 0 and p[1] <= self.shape[0], ixs))
            start = ixs[0]
            end = ixs[1]

        return [start, end]
