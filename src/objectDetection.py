#-----------------------------------------------------------------------------
# Copyright 2020 James Adam Campbell
#-----------------------------------------------------------------------------
# This file is part of MidlineTracker.
#
# MidlineTracker is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MidlineTracker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MidlineTracker.  If not, see <https://www.gnu.org/licenses/>
#-----------------------------------------------------------------------------

# Image processing
import cv2 as cv
# Geometry math and objects
import geometry as geo
# Loading videoframes
import videoBuffer as vb
import numpy as np
# Debugging
import time
from os import path

class ObjectDetection:
    """
    Use Grayscale threshold values to detect object

    This object holds the functions and parameters required to detect objects
    using an absolute threshold approach.

    This object also holds subclasses for the different detection methods.
    Each internal class will have the functions: grab_params() and bw() for
    getting the relevant parameters from the UI and creating a black and white
    image, respectively.

    Attributes
    ----------
    ui : midlineTracker.UI
        The main UI object for the application.  This is required to read user
        specified parameters.
    detect_method : Object
        This is one of the object detection internal classes which holds the
        algorithem for background subtraction.
    close_diamater : int
        Diameter of the kernel used for the close morphological operation.
    close_iterations : int
        Number of iterations for the close operation
    open_diamater : int
        Diameter of the kernel used for the open morphological operation.
    open_iterations : int
    min_object_size : int
    """

    debug = True

    def __init__(self, ui):

        if self.debug:
            print("...objectDetection.__init__")

        # init values to ui
        self.detect_method = None
        self.close_diameter = None
        self.close_iterations = None
        self.open_diameter = None
        self.open_iterations = None
        self.minBlob = None

        # Save GUI object
        self.ui = ui
        # Initalize detection settings
        self.grabParams()

    def grabParams(self):
        """
        Grab the object detection method from the UI and initialize a new
        detection internal class
        """
        if self.debug:
            print("...objectDetection.grabParams...")
            type_old = type(self.detect_method).__name__

        # ---------------- Init detection object ------------------------------
        value_changed = False
        if self.ui.rb_mog2.isChecked():
            print("......Set to: MOG2")

            # !!! NOT IMPLEMENTED YET !!!
        elif self.ui.rb_timeOffset.isChecked():
            if "TimeOffset" != type_old:
                print("......Set to: Time offset")
                self.detect_method = self.TimeOffset(self.ui)
                value_changed = True

        elif self.ui.rb_grayscaleThreshold.isChecked():
            if "Grayscale" != type_old:
                print("......Set to: Grayscale Threshold")
                self.detect_method = self.Grayscale(self.ui)
                value_changed = True

        else:
            raise ValueError("No valid detection method selected in UI")

        value_changed = self.detect_method.grabParams() or value_changed

        # ---------------- Get cleaning params from UI ------------------------

        if self.ui.cb_close.isChecked():
            if self.close_diameter != self.ui.sb_close_diameter.value():
                self.close_diameter = self.ui.sb_close_diameter.value()
                value_changed = True
            if self.close_iterations != self.ui.sb_close_iterations.value():
                self.close_iterations = self.ui.sb_close_iterations.value()
                value_changed = True
        else:
            if self.close_diameter != 0:
                self.close_diameter = 0
                value_changed = True
            if self.close_iterations != 0:
                self.close_iterations = 0
                value_changed = True

        if self.ui.cb_open.isChecked():
            if self.open_diameter != self.ui.sb_open_diameter.value():
                self.open_diameter = self.ui.sb_open_diameter.value()
                value_changed = True
            if self.open_iterations != self.ui.sb_open_iterations.value():
                self.open_iterations = self.ui.sb_open_iterations.value()
                value_changed = True
        else:
            if self.open_diameter != 0:
                self.open_diameter = 0
                value_changed = True
            if self.open_iterations != 0:
                self.open_iterations = 0
                value_changed = True

        # Make perfect circular kernels for morphological operations
        def makeKernel(diameter):
            # Init numpy array
            kernel = np.zeros((diameter, diameter), np.uint8)
            radius = diameter / 2
            for x in range(0, kernel.shape[1]):
                for y in range(0, kernel.shape[0]):
                    if (x + 0.5 - radius) ** 2 + \
                            (y + 0.5 - radius) ** 2 <= radius**2:
                        kernel[y, x] = 255
            return kernel

        if value_changed:
            if self.debug:
                print("......Kernels regenerated")
            self.kernel_open = makeKernel(self.open_diameter)
            self.kernel_close = makeKernel(self.close_diameter)

        #print("kernel open:", self.kernel_open)
        #print("kernel close:", self.kernel_close)

        if self.minBlob != self.ui.sb_minBlobArea.value():
            self.minBlob = self.ui.sb_minBlobArea.value()
            value_changed = True

        if self.debug and value_changed:
            print("......value changed")

        return(value_changed)

    def detectObject(self, ui, crop):
        """
        Detect moving object

        This function will run the user selected algorithem for object
        detection followed by cleaning operations.

        Parameters
        ----------
        ui : midlinetracker.UI

        crop : numpy.array()
            A cropped image representing the tracking box area of the video
            frame.

        Return
        ------
        bw : numpy.array()
            The raw black and white image returned from the motion detection
            method.
        bw_clean : The bw image with cleaning operations (open and close)
            applied to it.
        cntr : geometry.point
            The arithmetic mean of the x and y positions of the cleaned pixels.
        """

        if self.debug:
            print("...objectDetection.detectObject...")
            clk = time.time()

        # Convert to grayscale
        bw = cv.cvtColor(crop, cv.COLOR_BGR2GRAY)
        # Convert to bw
        bw = self.detect_method.bw(bw)
        bw_clean = bw.copy()
        # ----- Close image
        if self.close_iterations != 0:
            bw_clean = cv.morphologyEx(bw_clean, cv.MORPH_CLOSE,
                                       self.kernel_close,
                                       iterations=self.close_iterations)

        # Open image
        if self.open_iterations != 0:
            bw_clean = cv.morphologyEx(bw_clean, cv.MORPH_OPEN,
                                       self.kernel_open,
                                       iterations=self.open_iterations)

        # Remove small groups
        bw_clean = self.removeContours(bw_clean)

        # Calculate center of detected pixels
        idx_arr = np.argwhere(bw_clean)

        if bw_clean is None:
            # If no pixels detected, return center point of frame
            cntr = geo.point(x=np.floor(bw.shape[1]/2),
                             y=np.floor(bw.shape[0]/2))
            bw_clean = bw.copy()
        else:
            cntr = geo.point(
                y=int(round(np.mean(idx_arr[:, 0]))),
                x=int(round(np.mean(idx_arr[:, 1]))))

        if self.debug:
            print("...objectDetection.detectObject Timer: " +
                str(np.floor((time.time()-clk)*1000)) + "ms")

        return bw, bw_clean, cntr

    def removeContours(self, bw):
        """
        Remove all groups of contigious detected pixels leaving only the
        largest contour remaining.

        Parameters
        ----------
        image : numpy.array()
            Input image to remove small blobs from.

        Return
        ------
        image : numpy.array()
            Input image with small blobs removed.
        """
        if self.debug:
            print("...objectDetection.removeContours...")

        # Find contours in binary image
        contours = cv.findContours(bw, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)[0]

        if len(contours) == 0:
            # Exit early if no countours (i.e. no pixels) detected.
            return

        # Loop through contours and find largest one or those that exceed min
        # blob size
        contours_idx = []
        area_largest = 0
        area_idx = 0
        for i in range(0, len(contours)):
            area = cv.contourArea(contours[i])
            if area >= area_largest:
                area_largest = area
                area_idx = i
            if self.minBlob != 0 and self.minBlob < area:
                contours_idx = contours_idx + [i]

        # Add contour with largest area (may be added twice, but not a problem)
        contours_idx = contours_idx + [area_idx]

        # Wipe image and redraw only largest contour
        bw[:, :] = 0
        for i in range(0, len(contours_idx)):
            bw = cv.drawContours(bw, contours, contours_idx[i], 255, cv.FILLED)

        return(bw)

    # ------------------ Inner classes for detection methods ------------------

    class Grayscale:
        """
        Internal class for holding parameters for grayscale threshold detection
        of objects.

        Methods
        -------
        grab_params():
            Grab the relevant user settings from the UI to update the object
            attributes.
        bw():
            Convert image to black and white {0,255}.

        Attributes
        ----------
        threshold : int
            Threshold above, or below, which an pixel will be marked for object
            detection.  When set to 0, the threshold will be automatically
            determined using Otsu's method.
        threshType : cv.THRESH_BINARY or cv.THRESH_BINARY_INV
            An integer indicating if bright or dark objects are considered as
            detected.
        """

        debug = False
        __name__ = "Grayscale"

        def __init__(self, ui):
            """
            Initalize Greyscale threshold object detection algorithem.
            """
            if self.debug:
                print("...objectDetection.Grayscale...")

            # Save UI object
            self.ui = ui

            self.threshold = None
            self.threshType = None

        def grabParams(self):
            """
            Grab the relevant object detection parameters from the UI.
            """
            print("...objectDetection.Grayscale.grabParams...")
            # Get threshold value

            value_changed = False

            # Threshold
            if self.threshold != self.ui.sb_threshold_grayscale.value():
                self.threshold = self.ui.sb_threshold_grayscale.value()
                value_changed = True

            # Get threshold direction
            val = self.ui.cb_objectOnBackground.currentIndex()
            if val == 0:
                threshType = cv.THRESH_BINARY_INV
            elif val == 1:
                threshType = cv.THRESH_BINARY
            else:
                print("Threshold type:", val)
                raise ValueError("Invalid value for threshold type")

            if self.threshType != threshType:
                self.threshType = threshType
                value_changed = True
                if self.debug:
                    print("......Grayscale Params Changed")

            return value_changed

        def bw(self, frame):
            """
            Identify moving object in frame.

            Parameters:
                frame : Video frame to detect moving object on.

            Return
            ------
                Returns a binary frame holding the detected object.  Detected
                pixels ahve a value of 255 and non-detections 0.
            """
            if self.debug:
                print("...objectDetection.Grayscale.bw...")

            if self.threshold == 0:
                thresh, im_bw = cv.threshold(frame, self.threshold, 255,
                                             self.threshType | cv.THRESH_OTSU)
            else:
                thresh, im_bw = cv.threshold(frame, self.threshold, 255,
                                             self.threshType)

            return(im_bw)

    class TimeOffset:
        """
        Internal class for holding parameters for grayscale threshold detection
        of objects where the background image is defined by a time offset.

        This is preferred in situations where the background is constant and
        the object to track is non-stationary.

        Methods
        -------
        grab_params():
            Grab the relevant user settings from the UI to update the object
            attributes.
        bw():
            Convert image to black and white {0,255}.

        Attributes
        ----------
        threshold : int
            Threshold above, or below, which an pixel will be marked for object
            detection.  When set to 0, the threshold will be automatically
            determined using Otsu's method.
        time_offset : int
            An integer giving the number of frames to use as the time-offset
        preferred_reference : int
            0 or 1, indication wether the reference frame for background
            subtraction should be taken before or after the current frame,
            respectively.
        """

        debug = False
        __name__ = "TimeOffset"

        def __init__(self, ui):
            """
            Initalize TimeOffset object detection algorithem.
            """

            if self.debug:
                print("...objectDetection.TimeOffset.__init__...")

            # Save UI object
            self.ui = ui

            self.threshold = None
            self.preferred_reference = None
            self.time_offset = None

            # load new video
            if self.ui.video2 is not None:
                # Stop thread on existing video buffer
                ui.video2.stop()

            try:
                # Hard code buffer to load 1 frames before and after current
                ui.video2 = vb.VideoBuffer(path.join(self.ui.currentDir,
                                                     self.ui.current_file),
                                           0, 1, verbose=False)
            except ValueError:
                ui.video2 = None
                ui.statusMessage(
                    "Cannot open video file (timeoffset tracking): " +
                    ui.current_file)

        def grabParams(self):
            """
            Grab the relevant object detection parameters from the UI.
            """
            if self.debug:
                print("...objectDetection.TimeOffset.grabParams...")

            value_changed = False

            # Get threshold value
            if self.threshold != self.ui.sb_threshold_timeOffset.value():
                self.threshold = self.ui.sb_threshold_timeOffset.value()
                value_changed = True

            # Get time offset
            try:
                time_offset = int(np.floor(float(
                    self.ui.le_referenceFrameOffset.text())))
            except ValueError:
                print("Cannot Convert value to int, defaulting time offset to 1")
                time_offset = 1

            if time_offset > np.ceil(self.ui.video.video_frames/2):
                print("Reference frame out of bounds.  Defaulting to minimum value")
                time_offset = 1

            if time_offset != self.time_offset:
                self.time_offset = time_offset
                self.ui.le_referenceFrameOffset.setText(str(self.time_offset))
                value_changed = True

            if self.time_offset < 1:
                raise ValueError(
                    "Reference frame Offset must be greater than 0.")

            self.preferred_reference = \
                self.ui.cb_preferredReferenceFrame.currentIndex()

            return value_changed

        def bw(self, gray):
            """
            Identify moving object in frame.

            Parameters:
                gray : Grayscale video frame to detect moving object on.

            Return
            ------
                Returns a binary frame holding the detected object.  Detected
                pixels ahve a value of 255 and non-detections 0.
            """
            if self.debug:
                print("...objectDetection.TimeOffset.bw...")

            # ------ Get ref frame number
            if self.preferred_reference == 0:
                ofst = -self.time_offset
            else:
                ofst = self.time_offset

            reference_frame = self.ui.current_frame + self.time_offset
            if reference_frame < 0 or \
                    reference_frame >= self.ui.video.video_frames:
                reference_frame = self.ui.current_frame - self.time_offset

            if self.debug:
                print("reference_frame:", reference_frame,
                      "current_frame:", self.ui.current_frame)

            # ------ Create background image
            if self.debug:
                clk = time.time()
            frame2 = self.ui.video2.request_frame(reference_frame)
            if self.debug:
                clk2 = time.time()
                print("Request reference frame: " +
                      str(np.floor((clk2-clk)*1e3)) +
                      " ms")

            #print("frame2\n", frame2)
            if self.ui.current_frame == self.ui.trackbox.start_frame:
                trkFrm = self.ui.trackbox.start_frame
            else:
                trkFrm = self.ui.current_frame - 1

            crop2 = self.ui.trackbox.crop(frame2, trkFrm)
            #print("crop2\n", crop2)

            gray2 = cv.cvtColor(crop2, cv.COLOR_BGR2GRAY)

            # ------ Image subtraction
            sub = cv.absdiff(gray, gray2)
            #print("sub:\n", sub)

            if self.threshold == 0:
                thresh, im_bw = cv.threshold(sub, self.threshold, 255,
                                             cv.THRESH_BINARY | cv.THRESH_OTSU)
            else:
                thresh, im_bw = cv.threshold(sub, self.threshold, 255,
                                             cv.THRESH_BINARY)

            #return(im_bw)
            return(im_bw)
