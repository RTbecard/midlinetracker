import numpy as np
import time


"""
Image Geometry Module

This holds a numer of functions specifically tailored for bitmap geometry
where objects are plotted in two dimensional positive integer space.
"""

# --------------------------- Module functions --------------------------------
def colVec(x, y):
    """
    Return a numpy array formatted as a 2D column vector.
    """

    return np.array([[x, y]], np.float).T

# --------------------------- Class declarations ------------------------------
class point:
    """
    This class holds the x and y integer coords of a point

    Attributes
    ----------
    x : int
        The x coordinate of the point.
    y : int
        The y coordinate of the point.

    Methods
    -------
    tuple():
        Return x, y coords as a tuple.
    """
    def __init__(self, x, y):
        """
        Parameters
        ----------
        x : int
            x position of point.
        y : int
            y position of point.
        """
        self.x = x
        self.y = y

    def __repr__(self):

        return("(" + str(self.x) + ", " + str(self.y) + ")")

    def __getitem__(self, key):
        """
        Add functionality for indexing point.

        Return the value resulting from: self.tuple()[key]
        """
        return self.tuple()[key]

    def copy(self):
        return point(self.x, self.y)

    def tuple(self):
        """
        Return a tuple holding the x and y positions, respectively.
        """
        return self.x, self.y

    def rowVector(self):
        """
        Convert the point to a row vector.
        """
        return(np.array([[self.x, self.y]]))

    def columnVector(self):
        """
        Convert the point to a column vector.
        """
        return(np.array([[self.x, self.y]])).T

class line:
    """
    A line object in 2-dimensional space.

    The line information is saved in point vector notation.

    Attributes
    ----------
    point : geometry.point
        A point located anywhere along the line.
    vector : numpy.array(shape = (2, 1), dtype=numpy.float)
        A column vector holding the direction of the line in unit length.
    """

    def __init__(self, point, vector):
        """
        Define a line using point-vector notation.

        Parameters
        ----------
        point : geometry.point
            A point located anywhere along the line.
        vector : numpy.array(shape = (2, 1), dtype=numpy.float)
            A column vector holding the direction of the line in unit length.
        """

        # Check that vector is unit length
        lngt = np.dot(vector.T, vector) ** 0.5
        if lngt != 1:
            # Convert to unit length
            vector = vector/lngt

        # Save line
        self.point = point
        self.vector = vector

    def __repr__(self):

        return("[p: (" + str(self.point.x) + ", " +
               str(self.point.y) +
               ") v: (" + str(self.vector[0,0]) +
               ", " + str(self.vector[1, 0]) + ")]")

    def standard(self):
        """
        Return a tuple of values representing the standard form coefficients
        of the line equation for use in solving systems of linear equations:

        ax + by = c

        Return
        ------
        (a, b, c)
        """

        a = -self.vector[1, 0]
        b = self.vector[0, 0]
        c = a*self.point.x + b*self.point.y

        return a, b, c

    def orthogonal(self, point):
        """
        Return a new line object that is orthogonal to the current line and
        passes through the given point.

        Parameters
        ----------
        point : geometry.point
            A point the new line must pass through.
        """

        # Make orthonal vector for new line
        vec_orth = np.zeros([2, 1])
        vec_orth[0, 0] = self.vector[1, 0]
        vec_orth[1, 0] = -self.vector[0, 0]

        # Return copy of new line object
        return line(point.copy(), vec_orth)

    def distanceToPoint(self, point):
        """
        Calculate the shortest distance between the provided point and the
        current line object.

        Where A + tN represents a line (upper case are vectors, lower case
        scalars) and P is the point to calculate the distance to, the shortest
        distance can be calculated via:

            (A - P) - ((A - P) \\cdot N ) N

        Parameters
        ----------
        point : geometry.point
            A point to calculate the shortest distance to.
        """

        p = point.columnVector()

        orgn = self.point.columnVector()

        # Calculate shortest vector between point and line
        dist_vector = (orgn - p) - np.dot(orgn - p, self.vector)*self.vector

        dist_len = np.dot(dist_vector.T, dist_vector) ** 0.5
        return(dist_len)

    def pixelsOnLine(self, shape):
        """
        Return an index of pixels which have centers within 1 unit distance
        from the line.

        This is intended for finding all pixels in a numpy array which are
        close to the line.

        Parameters
        ----------
        shape : (int, int)
            A tuple holding the height and width of the numpy array.

        Return
        ------
        A list of tuples holding (x, y) coordinates for each marked pixel the
        line is overlapping with.
        """

        height = shape[0]
        width = shape[1]

        #print("Point:", self.point)
        #print("Vector:", self.vector)

        # ------ Get x value range ------

        if(self.vector[1, 0] == 0):
            # ------ Horizontal line
            x_range = range(0, int(width))
        elif(self.vector[0, 0] == 0):
            # ------ Vertical line
            x_range = range(int(np.floor(self.point[0])),
                            int(np.floor(self.point[0] + 1)))
        else:
            # ------ Diagonal line
            # x-values when y = 0 and y = height
            lmda = -self.point[1]/self.vector[1, 0]
            x_min = self.point[0] + lmda*self.vector[0, 0]

            lmda = (height - self.point[1])/self.vector[1, 0]
            x_max = self.point[0] + lmda*self.vector[0, 0]

            # move min value to first element
            x_range = [x_min, x_max]
            x_range.sort()
            x_range = range(int(np.floor(max(0, x_range[0]))),
                            int(np.ceil(min(width, x_range[1]))))

        #print("x range:", x_range)

        idx = []  # init an empty list for saving tuples of positions
        # Loop through x range and ID highlighted pixels
        for x in x_range:
            # ------ Vertical line
            if self.vector[0, 0] == 0:
                if self.point[0] >= x and self.point[0] <= (x + 1):
                    rng = [0, height]
            # ------ Diagonal or horizontal lines
            else:
                # Calculate min y pixel in range
                lmda_min = (x - self.point[0]) / self.vector[0, 0]
                y_start = self.point[1] + lmda_min*self.vector[1, 0]

                # Calculate max y pixel in range
                lmda_max = (x + 1 - self.point[0]) / self.vector[0, 0]
                y_end = self.point[1] + lmda_max*self.vector[1, 0]

                # Sort y limits in ascending order
                rng = [y_start, y_end]
                rng.sort()
                rng[0] = np.floor(rng[0])
                rng[1] = np.ceil(rng[1])

                # Deal with y positions directly on the pixel border
                if rng[0] == rng[1]:
                    rng[1] = rng[1] + 1

                rng[0] = int(max(0, rng[0]))
                rng[1] = int(min(height, rng[1]))

            # Add indicies to list object
            idx = idx + list(map(lambda y: (y, x), range(rng[0], rng[1])))

        #print("Marked indicies:", idx)

        return(idx)

    def which(self, array):
        """
        Return pixel indicies in the array which intersect the line and return
        True for the provided condition.

        Parameters
        ----------
        array : numpy.array(shape=[:, :, 1], dtype=uint8)
            The numpy array holding the image.
        condition : function
            A function which given a uint8 value returns a bool.
        """

        # Mark pixels the line is intersecting

        #clk = time.time()
        idx = self.pixelsOnLine(array.shape)

        # Convert list index tuples to indexing array
        idx_arr = np.empty((len(idx), 2), dtype=np.int)
        idx_arr[:, :] = idx
        idx_arr = idx_arr

        # Use indexing array to subset pixels touching line
        array_sub = array[tuple(idx_arr.T)]

        # Check which values are 255 (i.e. detected pixels)
        idx_true = np.nonzero(array_sub == 255)[0]
        #print("idx_true\n", idx_true)

        # Return array holding y, x coords of detected pixels
        if len(idx_true) == 0:
            idx_inner = np.zeros((0, 2))
        else:
            idx_inner = np.take(idx_arr, idx_true, axis=0)

        # Return
        return idx_inner

    def drawLine(self, array, value):
        """
        Paints pixels in a numpy array which are overlapping with line.

        The parameter idx can be calculated using the function
        line.pixelsOnLine.

        Parameters
        ----------
        array : numpy.array
            An 3 dimensional array holding an image to draw the line over.
        idx : [(x, y), (x, y), ...]
            A list holding tuples of the x, y indicies of each pixel to paint.
        color : (int, ) or (int, int, int)
            Color or grayscale value to paint on the array image.
        """

        idx = self.pixelsOnLine(array.shape)

        for i in idx:
            array[i[1], i[0], :] = value

        return(array)

class lineSegment:
    """
    A line segment consisting of two points.

    Attributes
    ----------
    A : point
        The start of the line.
    B : point
        The end of the line
    """
    def __init__(self, A, B):
        """
        Parameters
        ----------
        A : point
            Start of line segment
        B : point
            End of line segment
        """

        self.A = A
        self.B = B

    def __repr__(self):
        return(str(self.A) + "--" + str(self.B))

    def standard_form(self):
        """
        Return the coefficients for the standard form of the line.

        The standard form is: ax + by = c.

        Return
        ------
        A tuple of coefficients (a, b, c) which can be used in solving linear
        equations (such as finding the intercept between two lines).
        """

        # Calculate coefficients for slope-intersect form
        # y = mx + b   slope-intercept
        # -mx + y = b  rearrange
        # ax + by = c  relabel coefficients
        if self.A.x == self.B.x:
            # Vertical line
            return((1, 0, self.A.x))
        else:
            # Sloped line
            m = (self.A.y - self.B.y)/(self.A.x - self.B.x)
            b = self.A.y - (self.A.x*m)
            return((-m, 1, b))


class polygon:
    """
    Holds multiple connected points which makeup a closed polygon.

    Points are expected to trace the perimiter of the polygon in a clockwise
    direction.

    Attributes
    ----------
    points : list(point, point, ...)
        A lsit of points which draw the clockwise polygon perimeter
    """

    def __init__(self, points):
        """
        Parameters
        ----------
        points : list(point, point, point, ...)
            A list of points tracing the perimiter of the polygone in a
            clockwise direction.
        """

        # Verify all list items are points
        for p in points:
            if not isinstance(p, point):
                raise ValueError("polygon expects a list of point objects.")

        # Save attribute
        self.points = points

    def lineSegments(self):
        """
        Convert polygon to a list of line segments

        Return
        ------
        lines : list(line, line, line, ...)
            A list of lines which makeup the closed polygon.
        """

        # Define start and end of each line
        lineStart = self.points
        lineEnd = [self.points[-1]] + self.points[0:-1]
        # init empty list object
        lines = []

        for idx in range(0, len(self.points)):
            lines = lines + [lineSegment(lineStart[idx], lineEnd[idx])]

        return lines
# --------------------------- Public functions --------------------------------

def intersect_lineLine(AB, CD):
    """
    Use Cramer's method to determine if line segments are intersecting

    Where Ax = b, the intersect is given by:
        det(A_i)/det(A),  i = 1, 2
    A_i is is the matrix A with the column i replaced with vector
    b.

    If the determinant of A is 0, no intersect is possible.

    If no intersect is found, a point(-1,-1) will be returned which is out of
    the image coordinate system.

    Parameters
    ----------
    AB : line
        First line segment.
    CD : line
        Second line segment.
    """
    eqns = np.array([AB.standard_form(),
                     CD.standard_form()])
    A = eqns[:, 0:2]
    c = eqns[:, 2, None]

    dtrm = np.linalg.det(A)

#    print(".........line-line intersect")
#    print("A:\n", A)
#    print("c:\n", c)
#    print("Determenant:", dtrm)

    if dtrm == 0:
        # Check if intersect is possible
        return(point(-1,-1))
    else:
        # --- Find intersect (Cramers rule)
        # Make column substitutions
        A1 = np.concatenate((c, A[:,1, None]), axis = 1)
        A2 = np.concatenate((A[:,0, None], c), axis = 1)

        ix = point(np.linalg.det(A1)/dtrm, np.linalg.det(A2)/dtrm)
        ix.x = int(round(ix.x))
        ix.y = int(round(ix.y))

#        print("AB:", AB)
#        print("CD:", CD)
#        print(ix.x <= max(AB.A.x, AB.B.x))

        # Check if intersect is within line segment bounds
        if ix.x >= min(AB.A.x, AB.B.x) and ix.x <= max(AB.A.x, AB.B.x) and \
            ix.x >= min(CD.A.x, CD.B.x) and ix.x <= max(CD.A.x, CD.B.x) and \
            ix.y >= min(AB.A.y, AB.B.y) and ix.y <= max(AB.A.y, AB.B.y) and \
            ix.y >= min(CD.A.y, CD.B.y) and ix.y <= max(CD.A.y, CD.B.y):
            return(ix)
        else:
            return(point(-1,-1))


def intersect_linePoly(AB, poly):
    """
    Check if a line intersects a closed polygon

    Parameters
    ----------
    AB : lineSegment
    poly : polygon

    Return
    ------
    True if the line intersects any of the closed polygon edges.
    """

    lineStart = poly.points
    lineEnd = [poly.points[-1]] + poly.points[0:-1]

#    print("......line-poly intersect")
#    print(lineStart)
#    print(lineEnd)

#    print("line:", AB)
    for idx in range(0, len(poly.points)):
        line = lineSegment(lineStart[idx], lineEnd[idx])
        ix = intersect_lineLine(AB, line)

#        print("polygon edge:", line)
#        print("Intersect:", ix)
        if ix.x != -1:
            return(True)
    return(False)


def intersect_polyPoly(polyA, polyB):
    """
    Test if two polygons intersect eachother

    Parameters
    ----------
    polyA : polygon
    polyB : polygon

    Return
    ------
    True if the polygons are intersecting.
    """

    for idx in range(0, len(polyA.points) - 1):
        ix = intersect_linePoly(lineSegment(polyA.points[idx],
                                     polyA.points[idx + 1]),
                                polyB)
        if ix == True:
            return(True)

    return(False)
# --------------------------- Private functions -------------------------------

