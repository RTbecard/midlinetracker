#-----------------------------------------------------------------------------
# Copyright 2020 James Adam Campbell
#-----------------------------------------------------------------------------
# This file is part of MidlineTracker.
#
# MidlineTracker is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MidlineTracker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MidlineTracker.  If not, see <https://www.gnu.org/licenses/>
#-----------------------------------------------------------------------------

# Read CLI arguemnts
import sys
import os
import time
# For getting correct pyinstaller paths
from os import path
# For building the GUI
from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import pyqtSignal
# Interfacing with UI
import ui_interface as intf
# Video preview navigation
import video_navigation as vn
import geometry as geo
import numpy as np
# Matplotlib
import plots
# For lock function to update statusbar
import threading
# plotting midline
import pyqtgraph as pg
import re
import cv2 as cv

print("Opening UI...")

# Version number for display in titlebar
VERSION = "0.1.1"

# ############## Load UI file
"""
base_path is required for pyinstaller.  It points to the folder in the
resulting package that holds files added with the --add-data argument.
"""
try:
    # PyInstaller creates a temp folder and stores path in _MEIPASS
    BASE_PATH = sys._MEIPASS
except Exception:
    BASE_PATH = os.path.abspath(".")

FILE_UI = path.join(BASE_PATH, "mainwindow.ui")


# ############## Setup main window initializer
class Ui(QtWidgets.QMainWindow):
    """
    Load Qt GUI from file

    Load GUI from Qt5 .ui file and initialize attributes.

    Attributes
    ----------
    currentDir : str
        The absolute path to the currently selected directory.
    appDir : str
        Absolute path the the directory that midlinetracker is located in. This
        is used as the default search directory when the app is launched.
    file_names : [str, str, ...]
        List of files found in the currentDir.
    file_batch : [str, str, ...]
        List of files marked for batch processing.
    file_prepared : [str, str, ...]
        The expected filename holding the tracking parameters such as trackbox
        definition, arena bounds, object detection and midline estimation
        parameters.
    file_tracked : [str, str, ...]
        The expected filename for the resulting tracking data.  This file will
        hold the final estimation of the user requested midline points.
    check_prepared : [bool, bool, ...]
        For each corresponding element in file_prepared, a value of True
        indicates that the file exists and settings can be loaded.
    check_tracked : [bool, bool, ...]
        For each corresponding element in file_tracked, a value of True
        indicates that the file exists and settings can be loaded.
    current_file : str
        The absolute path to the currently selected file.  This is the file
        that will be previewed in the GUI.
    current_prepared : bool
        If True, the currently selected file has saved settings data.
    current_tracked : bool
        If True, the currently selected file has saved tracking data.
    video : cv2.VideoReader
        This holds the VideoBuffer object used to load videos from disk.
        Videos are loaded via a seperate thread and frames are cached in
        memory for quick retrieval.
    frame_period : float
        The frame wait interval during playback.  i.e. the inverse of the
        playback frame rate.
    _playback : bool
        Switch for indicating playback mode (on/off).  This is shared among
        threads and should only be modified by calling the function
        togglePlaybackStatus.
    trackbox : trackingBox.Trackbox
        This object holds the settings for tracking the currently selected
        video.
    midline :
        !!!!!! TODO !!!!!!
    playback_daemon : threading.Thread
        Thread object running the video playback.  This thread also handles the
        tracking operations.
    draw_tracking_box : bool
        When True, the GUI is configured to capture mouse presses for defining
        the tracking box.
    draw_tracking_arena : bool
        When True, the GUI is configured to capture mouse presses for defining
        the tracking arena.
    set_bearing : bool
        When True, the GUI is configured to capture mouse presses for defining
        the tracking bearing.
    clicks : [geometry.point, geometry.point, ...]
        This stores captured mouse clicks when mouseBusy() is called.
    lock_message : threading.Lock
        Lock for controlling the update of status message on the GUI.
    lock_playback : threading.Lock
        Lock controlling the toggle of video playback on/off.
    lock_current_frame : threading.Lock
        Lock controlling the changing of the current_frame.
    lock_settings : threading.Lock
        Lock controlling the changing of object detection settings during
        playback.
    """

    # Signal/Slot mechanism for redrawing UI frames
    # Must be declared outside class functions
    sigRedrawFrameMain = pyqtSignal()
    sigRedrawFrameTracking = pyqtSignal()
    sigRedrawMidline = pyqtSignal(np.ndarray, np.ndarray, list, np.ndarray)

    def __init__(self):
        super(Ui, self).__init__()  # Call parent class init

        uic.loadUi(FILE_UI, self)   # Load UI from file

        # Set window title
        self.setWindowTitle("Midline Tracker v" + VERSION)

        # Paths
        self.appDir = os.path.dirname(os.path.realpath(__file__))
        self.currentDir = self.appDir

        self.canvasDrawn = False

        # Batch processing attributes
        self.file_names = None
        self.file_batch = None
        self.file_prepared = None
        self.check_prepared = None
        self.file_tracked = None
        self.check_tracked = None
        # Tracking variables
        self.track_file = False
        self.track_batch = False
        self.to_write = ""

        # Attributes of the currently selected file
        self.current_file = None
        self.current_frame = 0
        self.current_prepared = None
        self.current_tracked = None

        # Videoplayback
        self.video = None
        self.video2 = None # For time offset tracking
        self.frame_period = 1/24
        self._playback = False

        # Saved tracking data from session
        self.trackbox = None

        # Flags for entering tracking box and arena drawing mode
        self.draw_tracking_box = False
        self.draw_tracking_arena = False
        self.set_bearing = False

        # Link mouse click callback
        self.label_video.mousePressEvent = self.getClick
        self.clicks = None

        # Output video
        self.video_out = None

        # Locks for thread safe variable updating
        self.lock_message = threading.Lock()
        self.lock_playback = threading.Lock()
        self.lock_settings = threading.Lock()
        self.lock_current_frame = threading.Lock()
        self.lock_midlineDraw = threading.Lock()
        self.lock_trackButton = threading.Lock()

        # Signal/Slot mechanism for redrawing UI frame

        self.sigRedrawFrameMain.connect(self._redrawFrameMain)
        self.sigRedrawFrameTracking.connect(self._redrawFrameTracking)
        self.sigRedrawMidline.connect(self._redrawMidline)

        # ------ Launch main UI ------
        # Connect signals to interface functions (ui_interface.py)
        intf.connectUi(self)
        # Add matplotlib widget
        plots.addPlots(self)

        # Launch window
        self.show()

        self.fileSelectMode(True)

        # Video playback thread
        self.playback_daemon = threading.Thread(target=self._playbackLoop,
                                                daemon=True)
        self.playback_daemon.start()

    def keyPressEvent(self, e):
        """
        Capture keypresses over entire UI

        If the UI is selected and no text fields are being edited, key presses
        will be directed here.
        """
        vn.keyPress(self, e)

    def _redrawFrameMain(self):
        """
        Redraw pixmap in QLabel holding current video frame.
        """
        self.label_video.repaint()

    def _redrawFrameTracking(self):
        """
        Redraw pixmaps in QLabels holding current tracking frames.
        """
        self.label_crop.repaint()

        pg.QtGui.QApplication.processEvents()

    def _redrawMidline(self, bw, bw_clean, points_raw, points_clean):
        plots.update_canvas(self, bw, bw_clean, points_raw, points_clean)

    def getClick(self, event):
        """
        This function holds actions while the user clicks in the Qlabel
        containing the current video frame.

        This function is used for defining the tracking arena and box.

        Parameters
        ----------
        event
            Event object returned by Qt5 keypress callback.
        """
        # Get pixel positons relative to unscaled video size
        x = event.pos().x()*self.video.frame_width / \
            self.label_video.pixmap().width()
        y = event.pos().y()*self.video.frame_height / \
            self.label_video.pixmap().height()
        button = event.button()

        # Exit if click register is disabled
        if self.clicks is None:
            return

        # Show click info
        print(str(button) + " click: " + str(x) + ", " + str(y))

        # Add click position to register
        self.clicks = self.clicks + [geo.point(int(x), int(y))]
        print(self.clicks)

        print(self.draw_tracking_arena)

        if self.draw_tracking_box:
            """
            Use 2 mouse clicks to define the tracking box shape and starting
            position
            """
            self.statusMessage("Click the lower right corner" +
                               " of the tracking box")

            # If 2 points set, exit box setup
            if len(self.clicks) >= 2:
                # Define track box
                if self.clicks[0].x < self.clicks[1].x and \
                        self.clicks[0].y < self.clicks[1].y:

                    self.trackbox.setTrackbox(self.clicks[0], self.clicks[1],
                                              self.current_frame)

                    # Redraw frame
                    vn.showFrame(self, self.current_frame)

                self.mouseRelease()

        elif self.set_bearing:
            """
            Use 2 mouse clicks to set the bearing of the individual
            """
            self.statusMessage("Click the final position of individual")
            # If 2 points set, exit box setup
            if len(self.clicks) >= 2:
                # Update bearing field
                bearing = np.arctan2(self.clicks[1].y - self.clicks[0].y,
                                     self.clicks[1].x - self.clicks[0].x)
                self.le_bearing.setText(str(round(bearing, 1)))
                self.statusMessage("Bearing is set")
                self.mouseRelease()
                self.trackbox.setupParamsMidlineEstimation()

    def mouseRelease(self):
        """
        Release the UI from mouse click activities

        When using the mouse to define the tracking features or bearing,
        certain UI elements will be disabled.
        """
        self.draw_tracking_box = False
        self.draw_tracking_arena = False
        self.set_bearing = False
        self.pb_setTrackingBox.setEnabled(True)
        self.pb_setBearing.setEnabled(True)

        # Disable mouse click register
        self.clicks = None

    def mouseBusy(self):
        """
        Set the UI to capture mouse click activities

        When using the mouse to define the tracking features or bearing,
        certain UI elements will be disabled.
        """

        self.pb_setTrackingBox.setEnabled(False)
        self.pb_setBearing.setEnabled(False)

        # Init mouse click register to empty list
        self.clicks = []

    def statusMessage(self, message):
        """
        Thread safe changing of status message on bottom of UI.

        The status message is used to provide feedback to operations like
        loading files.
        """

        self.lock_message.acquire()
        self.statusBar().showMessage(message)
        self.lock_message.release()

    def setCurrentFrame(self, video_frame_idx):
        """
        Thread safe setting of the current video frame.

        Video playback occurs on a seperate thread, hence the need to implement
        a thread safe updating of the current frame.

        Parameters
        ----------
        video_frame_idx : int
            0-based index for the video frame from the selected video file.
        """
        self.lock_current_frame.acquire()
        self.current_frame = video_frame_idx
        self.lock_current_frame.release()

    def togglePlaybackStatus(self):
        """
        Thread safe switching of playback status

        Playback runs on a background thread which continually calls the next
        frame.

        Parameters
        ----------
        status : bool
            When True, playback is running.  When false, the playback loop
            rests.
        """
        self.track_file = False

        if self._playback:
            status = False
        else:
            status = True

        # Disable tracking loop
        self.tracking_loop = False

        self.lock_playback.acquire()
        self._playback = status
        self.lock_playback.release()

        print("Playback status:", self._playback)

    def playbackTracking(self):
        """
        Reset frame to start position and run playback of the video region
        where an object is being tracked.  This will pool until the user stops
        playback.
        """
        self.track_file = False

        # Stop playback
        self.lock_playback.acquire()
        self._playback = False
        self.lock_playback.release()

        # Disable tracking loop
        self.tracking_loop = True

        # Reset to start frame
        if self.trackbox.start_frame is None:
            self.statusMessage("No trackbox has been defined yet.")
        else:
            # Load first frame
            vn.showFrame(self, self.trackbox.start_frame)

            # Start playback
            self.lock_playback.acquire()
            self._playback = True
            self.lock_playback.release()

        print("Playback status:", self._playback)

    def arr2str(self, frame, arr):
        out = ""
        for i in range(0, arr.shape[0]):
            out = out + str(frame) + ", " +  str(i) + ", " + \
            str(round(arr[i, 0], 2)) + ", " + \
            str(round(arr[i, 1], 2)) + "\n"

        return out

    def _playbackLoop(self):
        """
        Thread loop to control playback.

        When self._playback is True, playback is running at the specified FPS.
        When False, playback loop is sleeping.  Playback will run until the end
        of video or until the trackbox stops tracking an object.
        """

        print("Playback loop started")

        # time stamp of last frame display (seconds)
        last_read = None

        # True when an object is being tracked
        self.tracking = False

        while True:
            # Lock playback so object detection settings cant change during
            # frame loading
            if self._playback and \
                    self.video is not None and \
                    self.current_frame is not None:

                # Playback video
                frame_next = self.current_frame + 1

                # If end of video or end of tracking loop
                if frame_next >= self.video.video_frames or \
                        (self.tracking_loop and not
                         self.trackbox.tracked(self.current_frame)):

                    if self.track_file:

                        # =========== stop and save contents to disk
                        print("writing to:", self.current_tracked)
                        text_file = open(path.join(self.currentDir,
                                                   self.current_tracked), "w")
                        text_file.write(self.to_write)
                        text_file.close()

                        # Stop playback if end of video is reached
                        self.togglePlaybackStatus()
                        self.statusMessage("Finished Tracking File")

                        if self.file_batch:
                            # =======  open next video batch file and start
                            # tracking

                            print("Open next file for batch processing")
                            self.file_batch_idx += 1

                            if(self.file_batch_idx < len(self.file_batch)):
                                # Load next vid
                                self.table_files.setCurrentCell(
                                    self.file_batch[self.file_batch_idx], 2)
                                # Press track file button
                                self.pb_trackFile.click()
                            else:
                                print("Batch Tracking Complete")
                                self.track_batch = False
                                self.trackMode(False)

                        self.trackMode(False)

                    elif self.tracking_loop:
                        # Return to start frame
                        vn.showFrame(self, self.trackbox.start_frame)
                    else:
                        # Stop playback if end of video is reached
                        self.togglePlaybackStatus()
                else:
                    # Wait to match frame rate
                    if last_read is not None:
                        wait = max(self.frame_period -
                                   (time.time() - last_read), 0)
                        time.sleep(wait)
                    last_read = time.time()
                    # Load next frame
                    vn.showFrame(self, frame_next)
            else:
                # Sleep while not in playback mode
                time.sleep(0.1)

        print("Playback loop exited")

    def fileSelectMode(self, status):
        self.scrollArea.setEnabled(not status)
        self.pb_playStop.setEnabled(not status)
        self.pb_loopTracking.setEnabled(not status)
        self.cb_exportTrackingVideos.setEnabled(not status)
        self.pb_saveSetup.setEnabled(not status)
        self.pb_reloadSetup.setEnabled(not status)
        self.pb_trackBatch.setEnabled(not status)
        self.pb_setTrackingBox.setEnabled(not status)
        self.pb_setBearing.setEnabled(not status)
        self.pb_markStopframe.setEnabled(not status)
        self.sb_fps.setEnabled(not status)
        self.label_fps.setEnabled(not status)
        self.pb_trackFile.setEnabled(not status)

    def trackMode(self, status):
        self.scrollArea.setEnabled(not status)
        self.pb_playStop.setEnabled(not status)
        self.pb_loopTracking.setEnabled(not status)
        self.cb_exportTrackingVideos.setEnabled(not status)
        self.pb_saveSetup.setEnabled(not status)
        self.pb_reloadSetup.setEnabled(not status)
        self.pb_trackBatch.setEnabled(not status)
        self.tab_files.setEnabled(not status)
        self.pb_setTrackingBox.setEnabled(not status)
        self.pb_setBearing.setEnabled(not status)
        self.pb_markStopframe.setEnabled(not status)
        self.sb_fps.setEnabled(not status)
        self.label_fps.setEnabled(not status)

        if status is True:
            print("Open video output")
            # Close vidoeWriter object if previously initialized
            if self.video_out is not None:
                if self.video_out.isOpened():
                    self.video_out.release()
            self.pb_trackFile.setText("Cancel Tracking")

            # ------ Init new object
            # output file name
            if self.cb_exportTrackingVideos.isChecked():
                match = re.match("(^.*)\\.(.+)$", self.current_file)
                file_out = match.group(1) + "_tracked" + ".avi"
                file_out_full = os.path.join(self.currentDir, file_out)
                print("Video writer file:", file_out_full)
                frcc = cv.VideoWriter.fourcc('M', 'J','P','G')
                self.video_out = cv.VideoWriter(
                    file_out_full,
                    frcc,
                    self.video.fps,
                    (self.video.frame_width, self.video.frame_height))
                print("Video writer opened:", self.video_out.isOpened())
        else:
            if self.video_out is not None:
                print("Close video output")
                self.video_out.release()
            self.pb_trackFile.setText("Track File")

    def resizeEvent(self, event):
        """
        A redefinition of the resize callback to reload frame after resizing.
        """

        # Only apply resize when lock is free
        # Allows to resize during playback mode
        if not self.lock_settings.locked():
            self.lock_settings.acquire()
            # Resize window
            QtWidgets.QMainWindow.resizeEvent(self, event)
            # Redraw frame
            self._redrawFrameMain()
            self._redrawFrameTracking()

            # Pause playback when resizing
            ply = self._playback
            self._playback = False
            self.lock_settings.release()

            vn.showFrame(self, self.current_frame)
            # Refresh pyqtplot
            pg.QtGui.QApplication.processEvents()

            self.lock_settings.acquire()
            self._playback = ply
            self.lock_settings.release()

if __name__ == '__main__':

    APP = QtWidgets.QApplication(sys.argv)
    WINDOW = Ui()

    # Exit program when UI closes
    sys.exit(APP.exec_())
