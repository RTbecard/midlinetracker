#!/bin/bash

set -e

echo "============ Installing Ubuntu Dependancies"
apt install -y p7zip-full

echo "============ Installing Python Dependancies"
#pip3 install https://github.com/pyinstaller/pyinstaller/tarball/develop
pip3 install PyInstaller==3.6
pip3 install scipy==1.2.1 opencv-python==4.1.2.30 tornado==6.0.3 \
	pyqtgraph==0.10.0 dill==0.3.1.1 numpy==1.17.1 PyQt5==5.14.0
pip3 install --upgrade 'setuptools<45.0.0'

echo "============ Packaging via pyinstaller"
pyinstaller -y --clean --noconsole \
	--add-data 'mainwindow.ui:.' \
	--hidden-import numpy.random.common \
	--hidden-import numpy.random.bounded_integers \
	--hidden-import numpy.random.entropy \
	./midlinetracker.py

mv dist midlinetracker

echo "============ Compressing midlinetracker"

7z a midlinetracker.7z ./midlinetracker
