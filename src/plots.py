#-----------------------------------------------------------------------------
# Copyright 2020 James Adam Campbell
#-----------------------------------------------------------------------------
# This file is part of MidlineTracker.
#
# MidlineTracker is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MidlineTracker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MidlineTracker.  If not, see <https://www.gnu.org/licenses/>
#-----------------------------------------------------------------------------

import numpy as np

#from PyQt5 import QtWidgets
from pyqtgraph.Qt import QtWidgets
import pyqtgraph as pg
import midlineEstimation as me


"""
Holds functions for creating and modifying midline plotting elements.
"""

debug = True

def addPlots(ui):
    """
    Add matplotli figure canvas to qt

    Call when creating qtmain window
    """
    print("...plots.addPlots()...")

    # Make fig canvas (qt layout widget)
    ui.midlineRaw_GraphicsLayout = pg.GraphicsLayoutWidget()
    ui.viewBox = ui.midlineRaw_GraphicsLayout.addViewBox(invertY=True,
                                                         lockAspect=True)
    #ui.viewBox.setAspectLocked(True)
    # Set widget size policy
    sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding,
                                       QtWidgets.QSizePolicy.Expanding)

    # Apply size policy to pyplot widget
    ui.midlineRaw_GraphicsLayout.setSizePolicy(sizePolicy)
    #ui.midlineRajw_RawImageWidget.setSizePolicy(sizePolicy)

    # Add fig as widget to ui
    vbox = QtWidgets.QVBoxLayout()
    vbox.addWidget(ui.midlineRaw_GraphicsLayout)
    ui.midlineRaw.setLayout(vbox)

    # Make image item
    ui.image = pg.ImageItem(border="w")
    ui.scatter_raw = pg.ScatterPlotItem()
    ui.scatter_clean = pg.ScatterPlotItem()
    ui.scatter_interp = pg.ScatterPlotItem()
    ui.lines = pg.PlotCurveItem()
    # Add to plot
    ui.viewBox.addItem(ui.image)
    ui.viewBox.addItem(ui.lines)
    ui.viewBox.addItem(ui.scatter_raw)
    ui.viewBox.addItem(ui.scatter_clean)
    ui.viewBox.addItem(ui.scatter_interp)

def reset_canvas(ui, bw, bw_clean, points_raw, points_clean):
    """
    Reset the canvas each time a trackbox is defined or updated.

    Resetting will redraw the gridlines used for pixel detection
    """

    if debug:
        print("...plots.reset_canvas...")

    if ui.trackbox.midline_estimation.ready():
        if debug:
            print("Drawing Raw Midline...")

        # Bearing line
        lines = ui.trackbox.midline_estimation.segment(
            ui.trackbox.midline_estimation.b_line)
        # Pixel detection lines
        lines_app = list(map(ui.trackbox.midline_estimation.segment,
                             ui.trackbox.midline_estimation.lines))
        for line in lines_app:
            lines.append(line[0])
            lines.append(line[1])

        #print("lines:\n", lines)
        lines_arr = np.array(lines)
        #print("lines_arr:\n", lines_arr)
        ui.lines.clear()
        ui.lines.setData(x=lines_arr[:, 0], y=lines_arr[:, 1],
                         pen=(150, 250, 150), connect="pairs")

        ui.canvasDrawn = True
        update_canvas(ui, bw, bw_clean, points_raw, points_clean)
    else:
        ui.canvasDrawn = False

def update_canvas(ui, bw, bw_clean, points_raw, points_clean):
    """
    """
    if debug:
        print("...plots.update_canvas...")

    if not ui.canvasDrawn:
        reset_canvas(ui, bw, bw_clean, points_raw, points_clean)
    else:
        # Background image
        ui.image.setImage(bw.T, autoLevels=False, levels=(0, 255))
        # raw points
        pts = np.array(points_raw)
        ui.scatter_raw.clear()
        if len(points_clean) > 1:
            ui.scatter_raw.setData(pos=pts, symbol="+",
                                pen=(255,100,100), brush=None)
            # Filtered points
            ui.scatter_clean.setData(pos=points_clean, symbol="o",
                                    pen=(0,150,0), brush=None)
            if len(ui.trackbox.midline_interp) == 0:
                ui.scatter_interp.setData([])
            else:
                ui.scatter_interp.setData(
                    pos=me.interpolate(points_clean,
                                    ui.trackbox.midline_interp),
                    symbol="o", brush=(0,0,200))
