#!/bin/bash

set -e

echo "============ Installing Ubuntu Dependancies"
apt install -y p7zip-full

echo "============ package_windows.bat "

# Change wine to win7 registered
#WINEPREFIX=~/.wine64 winetricks win7  

# Run pyinstaller
WINEPREFIX=~/.wine64 wine cmd /c "package_windows.bat" 

echo "============ Compresing midlinetracker.exe"

mv dist midlinetracker
7z a midlinetracker.7z ./midlinetracker

