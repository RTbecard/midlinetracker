import cv2 as cv

class imageProcessing:

    def __init__(self):

        # When False, frames will not be processed.
        self.settings_tracking_valid = False

        # ------ Init tracking settings
        self.target_trajectory = "stationary"

        self.mog2_history = 300
        self.mog2_thresh = 9

        self.timeOffset_offset = 10
        self.timeOffset_preferred_before = True

        self.grayscale_thresh = 0
        self.grayscale_thresh_background_black = False

        self.bw_minObjSize = 0
        self.bw_close_diameter = 5
        self.bw_close_iterations = 2
        self.bw_open_diameter = 5
        self.bw_open_iterations = 2

        # ------ Init spline settings
        self.settings_processing_valid = False

        self.spline_points = None




    def processFrame():
