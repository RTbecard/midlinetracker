#-----------------------------------------------------------------------------
# Copyright 2020 James Adam Campbell
#-----------------------------------------------------------------------------
# This file is part of MidlineTracker.
#
# MidlineTracker is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MidlineTracker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MidlineTracker.  If not, see <https://www.gnu.org/licenses/>
#-----------------------------------------------------------------------------

# For writing objects to file
import dill
from os import path
from PyQt5.Qt import QImage, QPixmap
from PyQt5.QtCore import Qt
import numpy as np
# midline tracker custom modules
import videoBuffer as vb
import trackingbox as tb
import geometry as geo
import plots
import time
import midlineEstimation as me
import cv2 as cv

"""
Video Navigation Functions

This holds code relevant for loading and navagating video files
"""

debug = True

def keyPress(ui, e):
    """
    Catch keypresses on GUI

    This will capture all keypresses on the base GUI and execute the relevant
    function.
    """

    # Get text value of key
    key = e.text()
    # print(e.text())

    def slideFrames(n):
        """
        Slide to video frame and show on GUI

        Parameters
        ----------
        n : int
            The number of frames to move ahead.  Negative values will load
            previous frames.
        """

        # Calculate frame indesx to jump to
        video_frame_idx = ui.current_frame + n
        # Jump to frame
        jumpToFrame(video_frame_idx)

    def jumpToFrame(video_frame_idx):
        """
        Jump to video frame and show on GUI

        Parameters
        ----------
        idx : int
            The 0-based frame index to jump to.
        """

        if ui.lock_settings.locked() is not True:
            # Check that frame is within video
            if video_frame_idx < 0:
                video_frame_idx = 0
            elif video_frame_idx >= ui.video.video_frames:
                video_frame_idx = ui.video.video_frames - 1

            # Request frame
            showFrame(ui, video_frame_idx)

    # Video navigation and playback
    if key == "z":
        slideFrames(-1)
    elif key == "x":
        slideFrames(1)
    elif key == "a":
        slideFrames(-10)
    elif key == "s":
        slideFrames(10)
    elif key == "q":
        slideFrames(-100)
    elif key == "w":
        slideFrames(100)
    elif key == "1":
        slideFrames(-1000)
    elif key == "2":
        slideFrames(1000)
    elif key == "p":
        ui.togglePlaybackStatus()

def loadVideo(ui):
    # Stop playback
    ui.lock_settings.acquire()
    ui._playback = False
    ui.lock_settings.release()
    print("Playback stopped for loading new video.")

    # stop time diff video reader
    if ui.video2 is not None:
        ui.video2.stop()
        ui.video2 = None
    # Reset tracking and midline datasets
    ui.trackbox = None
    ui.midlineDetection = None

    # Load video
    try:
        # Hard code buffer to laod 100 frames before and after current
        ui.video = vb.VideoBuffer(path.join(ui.currentDir, ui.current_file),
                                  0, 1, verbose=False)
    except ValueError:
        ui.video = None
        ui.statusMessage("Cannot open video file: " + ui.current_file)

    # Reset current frame to video start
    ui.current_frame = 0

    if ui.video is not None:
        # Update status message (user feedback)
        ui.statusMessage(ui.current_file + " opened.")

        # Init empty track box object
        ui.trackbox = tb.Trackbox(ui)

        # Load tracking box if exists
        print("Checking for: " + ui.current_prepared)
        if path.exists(path.join(ui.currentDir, ui.current_prepared)):

            # Laod settings
            # must enable reload button so it can emit signal
            enbl = ui.pb_reloadSetup.isEnabled()
            ui.pb_reloadSetup.setEnabled(True)
            ui.pb_reloadSetup.click()
            ui.pb_reloadSetup.setEnabled(enbl)
            print("Loaded TrackingBox Data")

        # Load first frame into preview
        showFrame(ui, 0)
        ui.fileSelectMode(False)
    else:
        ui.fileSelectMode(True)

def updateImage(qlabel, array, rescale=True):
    """
    Paint numpy array as an image onto a Qlabel's.

    By default, this function will rescale images to fit the Qlabel.

    Parameters
    ----------
    qlabel : QLabel
        Q Qlabel obejct to fill with pixmap image.
    array : numpy.array
        Image data to paint to frame, in the form of an opencv formatted numpy
        array.
    """
    # Convert to QImage
    shp = array.shape
    if len(shp) == 3:
        qImg = QImage(array, shp[1], shp[0], shp[2]*shp[1],
                      QImage.Format_RGB888)
    else:
        qImg = QImage(array, shp[1], shp[0], shp[1], QImage.Format_Grayscale8)

    # Target width and height
    w = qlabel.width()
    h = qlabel.height()

    # Update QLabel with new image
    qlabel.setPixmap(QPixmap.fromImage(qImg).scaled(w, h, Qt.KeepAspectRatio))

def showFrame(ui, video_frame_idx):
    """
    Show the selected video frame in the UI

    This will update the UI to show the requested frame from the currently
    selected video file.

    Parameters
    ----------
    ui : Ui
        The Ui object holding the interface for midline tracker
    video_frame_idx : int
        0-based index for the frame to show in the UI.
    """

    # Lock object detection settings during show frame.  Avoids seg fault from
    # threading.
    ui.lock_settings.acquire()

    # Exit early if no video is loaded
    if ui.video is None or ui.trackbox is None:
        ui.lock_settings.release()
        return

    if debug:
        clk = time.time()
    frame = ui.video.request_frame(video_frame_idx)
    if debug:
        clk2 = time.time()
        print("Request video frame: " + str(np.floor((clk2-clk)*1e3)) + " ms")

    # Update currentframe index
    ui.setCurrentFrame(video_frame_idx)

    if ui.track_file:
        trk = " !!! TRACKING !!!"
    else:
        trk = ""

    ui.statusMessage("File: " + ui.current_file +
                     ", Frame: " + str(ui.current_frame) +
                     "/" + str(ui.video.video_frames - 1) + trk)
    # We need the current frame updated before we attempt the timeoffset
    # tracking

    # ------------ Apply object detection ------------
    # Check if object detection can be done
    ck = ui.trackbox.tracked(video_frame_idx - 1) or \
        video_frame_idx == ui.trackbox.start_frame

    if ck:
        first_track_frame = video_frame_idx == ui.trackbox.start_frame

        if first_track_frame:
            # First frame of tracking
            idx = video_frame_idx
        else:
            # Next frames of tracking
            idx = video_frame_idx - 1

        # Crop the trackbox image
        crop = ui.trackbox.crop(frame, idx)

        # Apply object detection (identify pixels of moving object)
        bw, bw_clean, cntr_crop = ui.trackbox.object_detection.\
            detectObject(ui, crop)

        if first_track_frame:
            # Update object mass offset if first tracking frame
            ui.trackbox.setCenterOffset(cntr_crop)

        # Convert cntr from trackbox coords to whole frame coords
        cntr_offset = ui.trackbox.corners(idx, True)
        cntr_frame = geo.point(cntr_crop.x + cntr_offset.x,
                               cntr_crop.y + cntr_offset.y)

        if not first_track_frame:
            # Update tracking frame
            ui.trackbox.set(video_frame_idx, cntr_frame)

        # ------------ Estimate midline position ------------
        ui.trackbox.getMidline(bw_clean)

        if len(ui.trackbox.midline_points_clean) >= 2:
            pts = me.interpolate(ui.trackbox.midline_points_clean,
                                 ui.trackbox.midline_interp)

            if video_frame_idx == ui.trackbox.start_frame:
                fr = video_frame_idx
            else:
                fr = video_frame_idx - 1

            corner = ui.trackbox.corners(fr)[0]

            # ------------ Paint midline points to frame ------------
            pts[:, 0] = pts[:, 0] + corner.x
            pts[:, 1] = pts[:, 1] + corner.y

            if pts is not None:
                for i in range(0, pts.shape[0]):
                    frame = cv.circle(frame,
                                      (int(np.floor(pts[i, 0])),
                                       int(np.floor(pts[i, 1]))),
                                      3, (0, 255, 0), -1)
                    frame = cv.circle(frame,
                                      (int(np.floor(pts[i, 0])),
                                       int(np.floor(pts[i, 1]))),
                                      3, (255, 0, 0), 1)

            if ui.track_file:
                st = ui.arr2str(video_frame_idx, pts)
                ui.to_write = ui.to_write + st
            else:
                pts = None

            # ------------ Output proofing frame
            if ui.cb_exportTrackingVideos.isChecked():
                # Write frame to video out
                frame_out = frame.copy()

                # Draw midline points
                if pts is not None:
                    for i in range(0, pts.shape[0]):
                        frame_out = cv.circle(frame,
                                              (int(np.floor(pts[i, 0])),
                                               int(np.floor(pts[i, 1]))),
                                              3, (0, 255, 0), -1)
                        frame_out = cv.circle(frame,
                                              (int(np.floor(pts[i, 0])),
                                               int(np.floor(pts[i, 1]))),
                                              3, (255, 0, 0), 1)
                ui.video_out.write(frame_out)

    # ----- Draw frame(s) on UI
    idx_tab = ui.tab_widget.currentIndex()
    if idx_tab == 0:
        # Draw tracking box
        frame_dec = ui.trackbox.draw(frame.copy(), video_frame_idx)
        # Update UI image
        updateImage(ui.label_video, frame_dec)
        ui.sigRedrawFrameMain.emit()

    elif idx_tab == 1:
        if ck:
            # Update raw
            updateImage(ui.label_crop, crop)

            # Mark cleaned points on bw image
            idx1 = np.nonzero(bw)
            bw[idx1[0], idx1[1]] = 100
            idx2 = np.nonzero(bw_clean)
            bw[idx2[0], idx2[1]] = 255

            # ------------ Plot midline points
            # Calculate raw midline points
            #plots.update_canvas(ui, bw, bw_clean, ui.trackbox.midline_points_raw)
            if ui.trackbox.midline_points_raw is not None:
                ui.sigRedrawMidline.emit(bw, bw_clean,
                                         ui.trackbox.midline_points_raw,
                                         ui.trackbox.midline_points_clean)
        else:
            # Clear images
            ui.label_crop.clear()
            #ui.image.setImage(None)

        # Repaint labels (i.e. force the redraw)
        ui.sigRedrawFrameTracking.emit()

    ui.lock_settings.release()
