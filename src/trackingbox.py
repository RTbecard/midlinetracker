#-----------------------------------------------------------------------------
# Copyright 2020 James Adam Campbell
#-----------------------------------------------------------------------------
# This file is part of MidlineTracker.
#
# MidlineTracker is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MidlineTracker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MidlineTracker.  If not, see <https://www.gnu.org/licenses/>
#-----------------------------------------------------------------------------

# Save/load files
# import dill
# Data arrays
import numpy as np
import cv2 as cv
import re  # regular expressions

# Custom modules
import geometry as geo
import objectDetection as od
import midlineEstimation as me

class Trackbox:
    """
    This class object holds the attributes of the tracking box

    Attributes
    ----------
    start_frame : int
        The frame index which the tracking frame is initialized on.
    stop_frame : int
        Frame index to terminate object tracking.
    width : int
        Width of tracking box in pixels.
    height : int
        Height of tracking box in pixels.
    center : numpy.array((2, n), numpy.int)
        A 2 x n array, where n is the number of video frames, used to save the
        center of the tracking box over time.  Values of 0 indicate no tracking
        box is defined for this frame.
    arena : geometry.polygon
        A polygon indicating the bounds of the tracking arena.
    center_offset : geometry.point
        Difference between the center of mass of detected pixels in the first
        frame of tracking and the center of the track box.  This is used to
        maintain a consistent position of the trackbox relative to the
        individual.
    object_detection : midlineEstimation.MidlineEstimation
        Object for estimating and filtering points along the midline.
    """

    debug = True

    def __init__(self, ui):
        """
        Intiate a tracking box on the current video frame

        This object will hold the position of the tracking box during video
        playback.

        Parameters
        ----------
        ui : midlinetracker.Ui
            The mainwindow GUI object.
        """

        # Get current video frame as starting point
        self.start_frame = None
        self.stop_frame = None
        self.width = None
        self.height = None
        self.arena = None

        self.ui = ui

        # Init array for holding tracking box center
        ui.trackbox_center = np.zeros((2, ui.video.video_frames), np.int)
        self.center = ui.trackbox_center
        self.center_offset = [0, 0]

        # Save copy of VideoBuffer object
        self.video = ui.video

        # Init object detection
        self.object_detection = od.ObjectDetection(ui)

        # Init midline detection
        self.midline_estimation = me.MidlineEstimation()
        self.setupParamsMidlineEstimation()
        self.midline_points_raw = None
        self.midline_points_clean = None
        self.midline_interp = None

        # Define tracking colors
        if ui.video.frame_channels == 1:
            self.color1 = 125
            self.color2 = 200
            self.color3 = 50
        elif ui.video.frame_channels == 3:
            self.color1 = (255, 150, 150)
            self.color2 = (255, 50, 50)
            self.color3 = (50, 50, 255)

    def set(self, video_frame_idx, point):
        """
        Set trackingbox position for a given frame.

        If the tracking box does not intersect the tracking arena bounds, the
        center of the box will be saved and True will be returned.  Else, False
        will be returned and can be used as an indicator that this animal is
        exiting the tracking arena and tracking should be stopped.

        Parameters
        ----------
        video_frame_idx : int
            Video frame to set the trackbox center for.
        point : geometery.point
            The center of the tracking box location.
        """

        self.center[0, video_frame_idx] = point.x - self.center_offset[0]
        self.center[1, video_frame_idx] = point.y - self.center_offset[1]

        # Check if trackbox is still in bounds
        ext = self.extents(video_frame_idx)
        ck = ext['xmin'] < 0 or \
            ext['xmax'] > self.video.frame_width or \
            ext['ymin'] < 0 or \
            ext['ymax'] > self.video.frame_height

        # If new trackingbox position is out of frame, set to no frame position
        if self.stop_frame is not None:
            ck = ck or self.stop_frame <= video_frame_idx

        if ck:
            self.center[0, video_frame_idx] = 0
            self.center[1, video_frame_idx] = 0

    def extents(self, video_frame_idx):
        """
        Return the extents of the tracking box

        Tracking boxes will always be rectangles consisting of horizontal and
        vertical lines.

        Parameters
        ----------
        video_frame_idx : int
            0-based frame number to extract the tracking extents from.

        Return
        ------
        extents : {xmin : int, xmax : int, ymin : int, ymax : int}
            A dictionary list holding the bounds of the tracking box, or None
            if no tracking is available for this frame.
        """

        x = self.center[0, video_frame_idx]
        y = self.center[1, video_frame_idx]

        extents = {"xmax": int(np.floor(x + (self.width/2))),
                   "xmin": int(np.floor(x - (self.width/2))),
                   "ymax": int(np.floor(y + (self.height/2))),
                   "ymin": int(np.floor(y - (self.height/2)))}

        return extents

    def setCenterOffset(self, center_trackbox):
        """
        Set the center offset values.

        The trackbox will be positioned relative to this offset value.  This is
        useful if the object to be tracked has an uneven mass distribution in
        the tracking frame (such as a broad head and thin tail), as the center
        of mass of the detected object is used to define the position of the
        trackbox in the next frame.

        This offset will be used to calculate future positions of the track
        box.

        Parameters
        ----------
        center_frame : geometry.point
            The center of detected mass within the coordinate system of the
            track box.
        """

        self.center_offset[0] = center_trackbox.x - np.floor(self.width/2)
        self.center_offset[1] = center_trackbox.y - np.floor(self.height/2)

    def crop(self, frame, video_frame_idx):
        """
        Return the cropped image used for object tracking.

        Parameters
        ----------
        frame : numpy.array(:, :, :)
            A numpy array holding thw image to crop.
        video_frame_idx : int
            0-based index inidcating the frame number to take the tracking box
            bounds from.

        Return
        ------
        A numpy array holding a the array crop.
        """

        ext = self.extents(video_frame_idx)
        crop = frame[ext["ymin"]:ext["ymax"], ext["xmin"]:ext["xmax"], :]
        return crop.copy()

    def tracked(self, video_frame_idx):
        """
        Check if video frame has been tracked.

        A center point of 0,0 for the trackbox indicates an untracked frame.

        Parameters
        ----------
        video_frame_idx : int
            0-based frame index to check tracking data for.
        """
        if self.debug:
            print("...trackingbox.tracked...")
#        print(self.center[:, video_frame_idx])
#        print(self.center[0, video_frame_idx])

        if video_frame_idx is -1:
            return False
        elif self.center[0, video_frame_idx] != 0:
            return True
        else:
            return False

    def setTrackbox(self, upper_left, lower_right, start_frame):
        """
        Define Tracking Box

        If a tracking box is defined, this function will return a frame with
        it painted on.  If not, the origional frame will be returned.

        Parameters
        ----------
        lower_right : geometry.point
            Lower right corner, in pixels, of tracking box.
        upper_left : geometry.point
            Upper left corner, in pixels, of tracking box.
        """

        if self.debug:
            print("...trackingbox.setTrackbox")

        if lower_right.y >= self.ui.video.frame_height or \
                lower_right.x >= self.ui.video.frame_width or \
                upper_left.x <= 0 or \
                upper_left.y <= 0:

            self.ui.statusMessage("Invalid tracking box... try again")
            print("......Invalid tracking box")
            self.width = None
            self.height = None
            self.start_frame = None
            self.center[:, :] = 0
            return

        # Save start frame
        self.start_frame = start_frame
        # Get box size
        self.width = abs(lower_right.x - upper_left.x)
        self.height = abs(lower_right.y - upper_left.y)
        # Reset box centers
        self.center[:, :] = 0
        # Init center for current frame
        self.center[0, self.start_frame] = upper_left.x + \
            np.floor(self.width/2)
        self.center[1, self.start_frame] = upper_left.y + \
            np.floor(self.height/2)

        self.ui.statusMessage("Tracking box defined")

        self.setupParamsMidlineEstimation(True)

    def corners(self, video_frame_idx, single=False):
        """
        Get the upper left and lower right corners of the track box

        Parameters
        ----------
        video_frame_idx : int
            The video frame to extract the track box points from.
        single : bool
            When True, only the upper left corner is returned.  This is used to
            convert the center point of the trackbox space to the whole frame
            space.

        Return
        ------
        start : geometry.point, stop : geometry.point
            A tuple of the two points.
        """
        hw = np.floor(self.width/2)
        hh = np.floor(self.height/2)
        start = geo.point(self.center[0, video_frame_idx] - hw,
                          self.center[1, video_frame_idx] - hh)
        if single:
            return start
        else:
            end = geo.point(self.center[0, video_frame_idx] + hw,
                            self.center[1, video_frame_idx] + hh)
            return start, end

    def draw(self, frame, video_frame_idx):
        """
        Draw tracking box and arena on frame

        Parameters
        ----------
        frame : numpy.array(width, height, channels, video_frame_idx)
            Video frame to paint tracking box onto.
        video_frame_idx : int
            0-based index for the video frame to paint.
        """
        if self.debug:
            print("...trackingbox.draw...")
        # Draw track box
        if self.start_frame is not None:
            if self.center[0, video_frame_idx] != 0:
                # Get box corners
                start, end = self.corners(video_frame_idx)
                start = (int(start.x), int(start.y))
                end = (int(end.x), int(end.y))
                # Draw box
                frame = cv.rectangle(frame, start, end, self.color2, 3)
            else:
                # Get box corners
                start, end = self.corners(self.start_frame)
                start = (int(start.x), int(start.y))
                end = (int(end.x), int(end.y))

                # Draw box
                frame = cv.rectangle(frame, start, end, self.color1, 3)

        # Draw arena
        if self.arena is not None:
            # Convert poly points to a tuple of tuples
            pts = [np.array(list(map(lambda x: [x.x, x.y], self.arena.points)),
                            dtype=np.int32)]
            # Draw polygon arena
            frame = cv.polylines(frame, pts, True, self.color3, 3)

        # Draw bw image
        if self.arena is not None and self.center[0, video_frame_idx] != 0:
            print("Placeholder")

        return(frame)

    def setupParamsMidlineEstimation(self, chng_trk=False):
        """
        Update the parameters for midline estimation.

        Returns true if calues have changed and frame needs to be redrawn
        """

        if self.debug:
            print("...trackingbox.setupParamsMidlineEstimation...")

        # Only update settings if trackbox is defined
        if(self.height is not None):
            # ============ Filter properties ============
            # ------ Cutoff frequency
            cutoff = self.ui.sb_lowPassFilter.value()
            # Filter order
            order = self.ui.sb_order.value()
            # Check if values have changed
            # if chng == True, relload frame after update
            chng_flt = cutoff != self.midline_estimation.cutoff or \
                order != self.midline_estimation.order
            # Set filter properties
            if chng_flt :
                self.midline_estimation.setFilter(cutoff, order)

            # ========== Midpoint positions to estimate ==========
            # Get midline point locations
            chng_intrp = False
            npt = self.ui.sb_nTrackPoints.value()
            vals = list(np.array(range(0, npt + 1))/float(npt))

            if self.debug:
                print("......Midline Points:", vals)

            if self.midline_interp != vals:
                self.midline_interp = vals
                chng_intrp = True

            # ============ Raw Line Generation ============
            self.midline_estimation.setShape((self.height, self.width))
            chng_raw = False
            # Bearing
            txt = self.ui.le_bearing.text()
            val = re.fullmatch("^-?[0-9]+(\\.[0-9]*)?", txt)
            if val is not None:
                bearing = float(txt)
                if bearing <= -3.14 or bearing >= 3.14:
                    bearing = self.midline_estimation.bearing
                    self.ui.le_bearing.setText(str(bearing))
            else:
                bearing = self.midline_estimation.bearing
                self.ui.le_bearing.setText(str(bearing))

            if self.midline_estimation.bearing != bearing:
                self.midline_estimation.bearing = bearing
                chng_raw = True

            # Line spacing
            if self.midline_estimation.line_spacing != \
                    self.ui.sb_rawPointInterval.value() or \
                    self.midline_estimation.line_spacing != \
                    self.ui.sb_rawPointInterval.value():
                chng_raw = True
                self.midline_estimation.line_spacing = \
                    self.ui.sb_rawPointInterval.value()
                self.midline_estimation.final_points = vals

            if chng_raw or chng_trk:
                # Calculate detection lines
                self.midline_estimation.rawLines()
                # Force redraw of midline gridlines
                self.ui.canvasDrawn = False

            ret = chng_raw or chng_intrp or chng_flt or chng_trk

            if self.debug and ret:
                print("......Midline estimation parameters changed")

            return ret

    def getMidline(self, bw):
        if self.debug:
            print("...trackingbox.getMidline...")
        if self.midline_estimation.ready():
            print("......estimating midline")
            # Get rawmidline points
            self.midline_points_raw = self.midline_estimation.rawMidline(bw)
            # Get filtered points
            if len(self.midline_points_raw) > 1:
                self.midline_points_clean = \
                    self.midline_estimation.runFilter(self.midline_points_raw)
            else:
                self.midline_points_clean = np.array(self.midline_points_raw)
        else:
            self.midline_points_raw = None
            self.midline_points_clean = np.empty((0, 2))
