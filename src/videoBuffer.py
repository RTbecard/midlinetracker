#-----------------------------------------------------------------------------
# Copyright 2020 James Adam Campbell
#-----------------------------------------------------------------------------
# This file is part of MidlineTracker.
#
# MidlineTracker is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MidlineTracker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MidlineTracker.  If not, see <https://www.gnu.org/licenses/>
#-----------------------------------------------------------------------------

# Video handling
import cv2 as cv
import numpy as np
# Threading
import threading
import queue
import time


class VideoBuffer:
    """
    The VideoBuffer class maintains a video frame buffer on a new thread.

    This class uses opencv (import cv2) to load video frames into a memory
    buffer.  When a new frame is requested, the frame will be copied from the
    buffer if available.  If not available, the buffer will first load the
    frame from disk the return a copy.  When no frames are requested, the
    buffer will automatically load from disk a user specified number of frames
    before and after the last requested frame.

    Reading frames from a memory buffer which is continously populated in the
    background should allow for faster loading of requested video frames.

    Attributes
    ----------
    buff_before : int
        Number of frames before the last read frame to store in buffer.
    buff_after : int
        Number of frames after the last read frame to store in buffer.
    buff_size : int
        Number of frames the buffer can hold.
    fps : float
        Frame rate of the video file.
    frame_height : int
        Height of video frames in pixels.
    frame_width : int
        Width of video frames in pixels.
    frame_channels : int
        Number of channels in a video frame (i.e. color channels).
    frame_dtype : numpy.dtype
        Data type of returned frames.
    video_frames : int
        Number of frames in video file.
    q_return, q_request : queue.Queue
        Queue objects used to send and recieve video frame requests.
    buff : numpy.array
        The numpy array used to hold the buffered video frames.
    buff_ready : numpy.array(dtype = bool)
        A 1 dimensional array holding the loaded status of each buffer frame.
    video_frame_idx : int
        The last requested video frame from the buffer.
    thread : threading.thread
        Thread object holding the buffering process.
    opened : bool
        Logical indicating if the video was sucefully opened.
    verbose : bool
        When True, debug messages will be returned to the console indicating
        the buffer status after each action.

    Methods
    -------
    start()
        Intitiate the thread and start populating the buffer.  This is run
        automatically when the class object is constructed.
    stop()
        Stop and exit the thread running the buffer.
    request_frame(video_frame_idx)
        Return a copy of the buffer frame holding the requested video frame.
    """

    def __init__(self, video_file, buff_before, buff_after, verbose=False):
        """
        Create a VideoBuffer class object and start buffering.

        Parameters
        ----------
        video_file : str
            Absolute or relative path to the video file to be buffered.
        buff_before : int
            The number of frames to buffer before the last requested frame.
        buff_after : int
            The number of frames to buffer after the last requested frame.
        verbose : bool
            After each buffer action, print the operation and buffer status.
            This is used for debugging purposes.
        """
        # Load video
        try:
            self.vc = cv.VideoCapture(video_file)
            self.opened = self.vc.isOpened()
        except cv.error:
            self.opened = False

        # Verify video is opened
        if not self.opened:
            raise ValueError("Video file did not load!  " +
                             "Check path and codecs.")

        # Load first frame
        # We need to do this to know how many channels each frame has, as
        # there is no VideoCapture property for this.
        ret, frame = self.vc.read()
        if not ret:
            raise Exception("Error reading first frame from file")

        # ------ Init class attributes
        self.verbose = verbose
        # Save buffer size
        self.buff_before = int(buff_before)
        self.buff_after = int(buff_after)
        self.buff_size = int(buff_before + buff_after + 1)
        # Grab meta data
        self.fps = self.vc.get(cv.CAP_PROP_FPS)
        self.video_frames = int(self.vc.get(cv.CAP_PROP_FRAME_COUNT))
        self.frame_width = int(self.vc.get(cv.CAP_PROP_FRAME_WIDTH))
        self.frame_height = int(self.vc.get(cv.CAP_PROP_FRAME_HEIGHT))
        self.frame_channels = frame.shape[2]
        self.frame_dtype = frame.dtype
        self.file_name = video_file

        # ------ Init numpy array holding the buffer memory space
        self.buff = np.empty(frame.shape + (self.video_frames, ),
                             self.frame_dtype)
        # Make buffer status array
        # This holds the status (loaded/not loaded) for each buffer frame
        self.buff_ready = np.empty(self.buff_size, np.bool)
        self.buff_ready[:] = False
        # Mark start and center of buffer in numpy array
        self.array_idx_current = buff_before
        self.array_idx_start = 0

        # ------ Initialize queue objects
        # We'll use queues to pass infomration too and from the buffer thread
        # Holds list of frames waiting to be read from disk
        self.q_request = queue.Queue()
        # single frame mode initated to false
        self.q_single = False
        # Holds requested frame copy
        self.q_return = queue.Queue()

        # ------ Init thread
        # Set last requested frame index
        self.video_frame_idx = int(self.vc.get(cv.CAP_PROP_POS_FRAMES) - 1)
        # Add loaded frame to buffer
        self.buff[:, :, :, self.array_idx_current] = frame
        self.buff_ready[self.array_idx_current] = True

        # Start thread
        self.thread = threading.Thread(target=self._process, daemon=False)
        self.start()

    # ============ Public interface functions =================================

    def request_frame(self, video_frame_idx):
        """
        Request a single video frame from the buffer

        If the frame is already loaded in the buffer, a copy of the buffer
        array will be returned.  If the frame is not loaded, it will first be
        read from disk, loaded in the buffer, and then copied and returned.

        Parameters
        ----------
        video_frame_idx : int
            A 0-based index of the video frame to retrieve.  Invalid frame
            indicies will raise an error.
        """

        # Queue request for frame
        self.q_request.put(video_frame_idx)

        # Retrieve requested frame
        return(self.q_return.get())

    def status(self):
        """
        Return True if buffer thread is still running.
        """
        return(self._run)

    def stop(self):
        """
        Send stop signal to buffer thread.
        """
        self._run = False

        if self.verbose:
            if not self.thread.is_alive():
                print("Thread is already stopped.")
            else:
                print("Stop signal sent to thread.")

    def start(self):
        """
        Start the buffer thread.
        """
        # Run this loop until the thread is terminated.
        self._run = True
        if self.thread.is_alive():
            print("Thread is already running. Restarting Thread.")
        else:
            self.thread.start()

    # =============== Internal thread functions ===============================

    def _process(self):
        """
        Function that is called when the thread starts.
        """
        while(self._run):
            self._update()

    # This holds the thread actions.
    # I'm not hard coding this into the run loop for easier testing/debugging
    def _update(self):
        """
        Actions to take during an iteration of the thread loop.

        When a frame is requested, return a copy of the buffer frame.  When no
        frame is requested, fill the buffer with video frames.
        """
        # Check for new frame pull request
        if not self.q_request.empty():
            video_frame_idx = self.q_request.get()
            if video_frame_idx < 0 or video_frame_idx >= self.video_frames:
                raise ValueError("Requested frame is outside video range.")
            else:
                # Load frame
                self._buffer_frame_read(video_frame_idx)
                self.video_frame_idx = video_frame_idx
            # Show buffer info for debugging
            if self.verbose:
                self.print_buffer()
        # Check if buffer is full
        elif not self._buffer_is_full():
            # Put next frame in buffer
            self._buffer_frame_put()
            # Show buffer info for debugging
            if self.verbose:
                self.print_buffer()
        # If nothing to do, sleep for 10ms
        else:
            time.sleep(0.001)

    # ============== Index conversion functions ===============================

    # Convert video idx to buffer idx
    def _video2buff_idx(self, video_frame_idx):
        """
        Convert a video frame index to a buffer frame index
        """
        buff_frame_idx = video_frame_idx - self.video_frame_idx
        return(buff_frame_idx)

    # Convert video idx to buffer idx
    def _buff2video_idx(self, buffer_frame_idx):
        """
        Convert a buffer frame index to a video frame index
        """
        video_frame_idx = buffer_frame_idx + self.video_frame_idx
        return(video_frame_idx)

    # Convert buffer idx to array idx
    def _buff2array_idx(self, buff_frame_idx):
        """
        Convert a buffer frame index to a numpy array frame index
        """
        array_frame_idx = buff_frame_idx + self.array_idx_current
        if array_frame_idx >= self.buff_size:
            # Wrap around if value is too large
            array_frame_idx = array_frame_idx - self.buff_size
        if array_frame_idx < 0:
            # Wrap around if value is too low
            array_frame_idx = array_frame_idx + self.buff_size
        return(array_frame_idx)

    def _buff2ready_idx(self, buff_frame_idx):
        """
        Convert a buffer frame index to buffer loaded frame index
        """
        return(buff_frame_idx + self.buff_before)

    def _ready2buff_idx(self, ready_frame_idx):
        """
        Convert a buffer ready index to a buffer frame index
        """
        return(ready_frame_idx - self.buff_before)

    # ============= Internal buffer functions =================================

    def _buffer_frame_read(self, video_frame_idx):
        """
        Return a copy of the requested frame.

        Check's if the requested frame is in the buffer range then calls
        _buffer_frame_read().  If frame is outside buffer range, the buffer is
        first reset to center on the requested frame.
        """

        buff_frame_idx = self._video2buff_idx(video_frame_idx)
        # Check index is in buffer range and is loaded
        if (buff_frame_idx < -self.buff_before or
            buff_frame_idx > self.buff_after):
            # Reset buffer to center on new frame index
            self._buffer_reset(video_frame_idx)
            self._buffer_frame_get(0)
        else:
            self._buffer_frame_get(buff_frame_idx)

    def _buffer_frame_range(self):
        """
        Return a list holding the first and last buffer indicies that are
        within the video range.
        """
        # Mark buffer frames within video range
        video_frame_idx = list(map(lambda x: self._buff2video_idx(x),
                                   range(-self.buff_before,
                                         self.buff_after + 1)))
        # Return edges of buffer video range
        tmp = list(filter(lambda x: x >= 0 and
                          x < self.video_frames, video_frame_idx))

        return((self._video2buff_idx(tmp[0]),
                self._video2buff_idx(tmp[-1])))

    def _buffer_is_full(self):
        """
        Return True if all available buffer frames have been loaded.
        """
        # Check for empty buffer spots
        rng = self._buffer_frame_range()
        idx = slice((self._buff2ready_idx(rng[0])),
                    (self._buff2ready_idx(rng[1] + 1)))
        return(np.all(self.buff_ready[idx]))

    def _buffer_frame_put(self):
        """
        Put a video frame in the next unloaded buffer frame.
        """
        # Mark buffer frames within video range
        idx = self._buffer_frame_range()
        rg = range(idx[0], idx[1] + 1)

        if self.verbose:
            print("......put candidates: buffer", rg)

        # Get first unloaded video frame
        buff_frame_idx = list(filter(
            lambda x: not self.buff_ready[self._buff2ready_idx(x)], rg))[0]
        video_frame_idx = self._buff2video_idx(buff_frame_idx)
        array_frame_idx = self._buff2array_idx(buff_frame_idx)

        if self.verbose:
            print("......put video frame:", video_frame_idx,
                  "in buffer position", buff_frame_idx)

        self.buff[:, :, :, array_frame_idx] = \
            self._video_frame_read(video_frame_idx)
        self.buff_ready[self._buff2ready_idx(buff_frame_idx)] = True

    def _buffer_frame_is_ready(self, buffer_frame_idx):
        """
        Check if buffer frame is already loaded.
        """
        return(self.buff_ready[buffer_frame_idx])

    # Retrieve a frame from the buffer.
    # idx = 0 is equal to the frame indexed by video_frame_idx
    def _buffer_frame_get(self, buff_frame_idx):
        """
        Return a requested buffer frame within the buffer range.
        """

        # Cast to int
        buff_frame_idx = int(buff_frame_idx)
        if self.verbose:
            print("......get buffer idx:", buff_frame_idx)

        # ------ Update array start/end
        current = self._buff2array_idx(buff_frame_idx)
        start = self._buff2array_idx(-self.buff_before + buff_frame_idx)
        self.array_idx_current = current
        self.array_idx_start = start

        # ------ Update buffer referesh list
        if buff_frame_idx > 0:
            off_end = (self.buff_size - 1 - buff_frame_idx)
            self.buff_ready[:(off_end + 1)] = \
                self.buff_ready[buff_frame_idx:]
            self.buff_ready[(off_end + 1):] = False
        elif buff_frame_idx < 0:
            off_end = (self.buff_size + buff_frame_idx)
            self.buff_ready[(-buff_frame_idx):] = self.buff_ready[:(off_end)]
            self.buff_ready[:(-buff_frame_idx)] = False

        # update current video frame
        self.video_frame_idx = self._buff2video_idx(buff_frame_idx)

        # ------ If current frame is not loaded, load it
        if not self.buff_ready[self.buff_before]:
            if self.verbose:
                print("......Load buff(0)")
            self.buff[:, :, :, self._buff2array_idx(0)] = \
                self._video_frame_read(self.video_frame_idx)
            self.buff_ready[self.buff_before] = True

        # Add copy of buffered frame to queue
        self.q_return.put(self.buff[:, :, :, self._buff2array_idx(0)].copy())

    # Clear the buffer and reset the frame index
    def _buffer_reset(self, video_frame_idx):
        """
        Clear the buffer then recenter it on video_frame_idx().

        This function is called when a frame is requested that lies outside the
        buffer range.  This is typically followed by calling
        _buffer_frame_get().
        """
        # Reset buffer load queue
        self.buff_ready[:] = False
        # Reset array indexes
        self.array_idx_current = self.buff_before
        self.array_idx_start = 0

        # Load center frame
        self.buff[:, :, :, self.buff_before] = \
            self._video_frame_read(video_frame_idx)
        self.buff_ready[self.buff_before] = True

    # ============== Internal video functions =================================

    def _video_frame_read(self, video_frame_idx):
        """
        Load a video frame from the disk.
        """

        if self.verbose:
            print("...loading frame from file:", video_frame_idx)

        if video_frame_idx == (self.video_frame_idx + 1):
            # Load next frame
            return(self._video_frame_next())
        else:
            # ------ Jump and load frame
            # Move video capture frame index
            self.vc.set(cv.CAP_PROP_POS_FRAMES, video_frame_idx)
            # Load and return frame
            ret, frame = self.vc.read()
            if not ret:
                raise Exception("Error reading next frame from file")
            else:
                return(frame)

    # Load next frame from file
    def _video_frame_next(self):
        """
        Load the next video frame from disk.
        """
        # Read frame from file
        ret, frame = self.vc.read()

        if not ret:
            raise Exception("Error reading next frame from file")
        else:
            return(frame)

    # ============== Debugging ================================================

    # Proofing function for showing buffer status
    def print_buffer(self):
        """
        Show debugging information about the video buffer.
        """
        if self.verbose:
            print("video:", self.video_frame_idx,
                  "; buffer:", self._video2buff_idx(self.video_frame_idx),
                  "; array:", self._buff2array_idx(
                      self._video2buff_idx(self.video_frame_idx)))
            print("array start:", self.array_idx_start,
                  "; array center:", self.array_idx_current)
            print("Buffer queue:",
                  self.buff_ready[0:(self.buff_before)],
                  "|", self.buff_ready[self.buff_before], "|",
                  self.buff_ready[(self.buff_before + 1):])
            print("-----------------------------------------")
