echo off

echo "============ Install Python Dependancies"
:: Will only compile with pyinstaller 3.4 for windows version
:: When using 3.5 or 3.6, crashes without error message after unpacking the exe

:: pip3 install https://github.com/pyinstaller/pyinstaller/tarball/develop
pip3 install PyInstaller==3.6

pip3 install ^
	scipy==1.2.1 ^
	opencv-python==4.1.2.30 ^
	tornado==6.0.3 ^
	pyqtgraph==0.10.0 ^
	dill==0.3.1.1 ^
	numpy==1.17.1 ^
	PyQt5==5.14.0

pip3 install --upgrade 'setuptools<45.0.0'

::echo " ============ debug (one folder build)" 
::pyinstaller -D -y --clean --debug bootloader --debug imports --log-level=DEBUG --noupx ^
::	--add-data mainwindow.ui;. ^
::	--hidden-import codecs ^
::	.\midlinetracker.py
::
::REM Exit if error detected
::IF %ERRORLEVEL% NEQ 0 ( 
::	echo "!!!!!!  Failed debug build  !!!!!!"
::	EXIT /B %ERRORLEVEL%
::)


echo " ============ Package via pyinstaller" 
:: Py installer needs to run twice in order to enable debug mode.
:: See issue here https://github.com/pyinstaller/pyinstaller/issues/4034#issuecomment-558204841

:: --noconsole is a win10 specific flag.  Program will not launch without it
:: --console is required for debug messages to appear
pyinstaller -y --clean --noupx ^
	--add-data mainwindow.ui;. ^
	--console ^
	--hidden-import codecs ^
	--hidden-import numpy.random.common ^
	--hidden-import numpy.random.bounded_integers ^
	--hidden-import numpy.random.entropy ^
	.\midlinetracker.py

	REM Exit if error detected
IF %ERRORLEVEL% NEQ 0 ( 
	echo "!!!!!!  Failed distribution build  !!!!!!"
	EXIT /B %ERRORLEVEL%
)

