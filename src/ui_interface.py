#-----------------------------------------------------------------------------
# Copyright 2020 James Adam Campbell
#-----------------------------------------------------------------------------
# This file is part of MidlineTracker.
#
# MidlineTracker is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MidlineTracker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MidlineTracker.  If not, see <https://www.gnu.org/licenses/>
#-----------------------------------------------------------------------------

# Qt interface
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QFileDialog, QTableWidgetItem
from PyQt5 import QtCore
# File paths
import os
import re
# Saving settings
import dill

# Video select
import video_navigation as vn


def connectUi(ui):
    print("No UI connections")

    # Set UI response functions
    # These define the context dependant actions of the UI

    # Object Detection
    def selectTrackingAlgorithem():
        """
        Hide groupboxes which are not relevant to the currently selected object
        detection algorithem and reset tracking setings.
        """
        # Hide UI elements showing irrelevant options
        if ui.rb_grayscaleThreshold.isChecked():
            # Select Grayscale threshold
            ui.gb_grayscaleThreshold.setVisible(True)
            ui.gb_timeOffset.setVisible(False)
            ui.gb_mog2.setVisible(False)
        elif ui.rb_timeOffset.isChecked():
            # Select time offset background subtraction
            ui.gb_grayscaleThreshold.setVisible(False)
            ui.gb_timeOffset.setVisible(True)
            ui.gb_mog2.setVisible(False)
        elif ui.rb_mog2.isChecked():
            # Select MOG2 for background subtraction
            ui.gb_grayscaleThreshold.setVisible(False)
            ui.gb_timeOffset.setVisible(False)
            ui.gb_mog2.setVisible(True)

        updateParamMethod()

    # ------------------- File select table -----------------------------------

    def selectFolder():
        """
        Open a folder select UI and then repopulate the file select table
        """
        tmpDir = ui.appDir if ui.currentDir is None else ui.currentDir
        ui.currentDir = str(
            QFileDialog.getExistingDirectory(ui, "Select Directory", tmpDir))
        # Update folder field
        ui.le_folder.setText(ui.currentDir)
        # Update files table
        listFiles()

    # Populate QTableWidget
    def listFiles():
        """
        Populate file select table with files in current folder
        """
        if ui.currentDir == '':
            return

        # Save marked files
        updateBatch()

        # ------ Scan directory for working files
        # --- Scan for suitable files
        # Set filetype filter
        files = os.listdir(ui.currentDir)
        #print("Files:", files)

        if ui.le_filter.text() != "":
            filt = ".+\\." + ui.le_filter.text() + "$"
        else:
            filt = ".+\\..+$"
        #print("filt: " + filt)

        # Run extension filter
        ui.file_names = filter(lambda x: re.match(filt, x) is not None, files)
        # Remove tracking files from list
        ui.file_names = filter(lambda x:
                               re.match(".*\\.(mtbox|mttracker)$", x) is None,
                               ui.file_names)
        # Remove folders
        ui.file_names = list(
            filter(lambda x: os.path.isfile(os.path.join(ui.currentDir, x)),
                   ui.file_names))
        #print("file_names:")
        #print(ui.file_names)

        # --- Mark prepared files (have a defined tracking area)
        ui.file_prepared = map(lambda x: re.sub("\\.(?:.(?!\\.))+$",
                                                ".mtbox", x),
                               ui.file_names)
        ui.file_prepared = list(ui.file_prepared)
        ui.check_prepared = map(lambda x: os.path.exists(
            os.path.join(ui.currentDir, x)),
            ui.file_prepared)
        ui.check_prepared = list(ui.check_prepared)

        #print("file_prepared:")
        #print(ui.file_prepared)
        #print(ui.check_prepared)

        # --- Mark files that have been tracked previously
        ui.file_tracked = map(lambda x: re.sub("\\.(?:.(?!\\.))+$",
                                               ".mttracker", x),
                              ui.file_names)
        ui.file_tracked = list(ui.file_tracked)
        ui.check_tracked = map(lambda x: os.path.exists(
            os.path.join(ui.currentDir, x)),
            ui.file_tracked)
        ui.check_tracked = list(ui.check_tracked)

        #print("file_tracked:")
        #print(ui.file_tracked)
        #print(ui.check_tracked)

        # Remove obsolete files from batch processing
        if ui.file_batch is not None:
            ui.file_batch = list(filter(lambda x: x in ui.file_names,
                                        ui.file_batch))

        # ------ Populate table widget
        # Set table dimensions
        if len(ui.file_names) > 0:
            ui.table_files.setRowCount(len(ui.file_names))
            ui.table_files.setColumnCount(3)
            ui.table_files.setHorizontalHeaderLabels(["Prepared",
                                                      "Tracked",
                                                      "File"])

            # Define unicode symbols for checkmark and cross
            # Loop through table rows
            str_yes = "<p align=\"center\"style=\"color:green;\">&#x2713;</p>"
            str_no = "<p align=\"center\"style=\"color:red;\">&#x2717;</p>"
            for row in range(0, len(ui.file_names)):
                # Set chackable file name
                chkbx = QTableWidgetItem(ui.file_names[row])
                chkbx.setFlags(QtCore.Qt.ItemIsUserCheckable |
                               QtCore.Qt.ItemIsEnabled |
                               QtCore.Qt.ItemIsSelectable)
                if ui.file_batch is not None:
                    if ui.file_names[row] in ui.file_batch:
                        chkbx.setCheckState(QtCore.Qt.Checked)
                    else:
                        chkbx.setCheckState(QtCore.Qt.Unchecked)
                else:
                    chkbx.setCheckState(QtCore.Qt.Unchecked)
                # Update batch list when checkbox changes state
                ui.table_files.setItem(row, 2, chkbx)

                # Set box and tracker checks
                if ui.check_prepared[row]:
                    ui.table_files.setCellWidget(row, 0,
                                                 QtWidgets.QLabel(str_yes))
                else:
                    ui.table_files.setCellWidget(row, 0,
                                                 QtWidgets.QLabel(str_no))
                if ui.check_tracked[row]:
                    ui.table_files.setCellWidget(row, 1,
                                                 QtWidgets.QLabel(str_yes))
                else:
                    ui.table_files.setCellWidget(row, 1,
                                                 QtWidgets.QLabel(str_no))

            ui.table_files.resizeColumnsToContents()
        else:
            ui.table_files.setRowCount(0)
            ui.table_files.setColumnCount(0)
            ui.current_file = None

    # Select file from table with mouse
    def selectFile(currentRow, currentColumn, previousRow, previousColumn):
        """
        The clicked item from the file table becomes the current_file shown
        in the video previews.
        """
        # Stop playback

        ui.lock_settings.acquire()
        ui._playback = False
        ui.lock_settings.release()

        updateBatch()
        ui.current_file = ui.file_names[currentRow]
        ui.current_prepared = ui.file_prepared[currentRow]
        ui.current_tracked = ui.file_tracked[currentRow]
        #print("current_file: " + os.path.join(ui.currentDir, ui.current_file))
        #print("current_prepared: " +
        #      os.path.join(ui.currentDir, ui.current_prepared))
        #print("current_tracked: " +
        #      os.path.join(ui.currentDir, ui.current_tracked))
        vn.loadVideo(ui)

    def updateBatch():
        """
        Update list of files marked for batch processing
        """
        ui.file_batch = []
        if ui.file_names is not None:
            for row in range(0, len(ui.file_names)):
                state = ui.table_files.item(row, 2).checkState()
                if state:
                    #ui.file_batch = ui.file_batch + [ui.file_names[row]]
                    ui.file_batch = ui.file_batch + [row]
        else:
            ui.file_batch = None

        print("file_batch: ")
        print(ui.file_batch)

    def markPrepared():
        """
        Mark all files in table for processing which have trackingbox files.
        """
        if ui.file_names is not None:
            for row in range(0, len(ui.file_names)):
                if ui.check_prepared[row]:
                    ui.table_files.item(row, 2).setCheckState(
                        QtCore.Qt.Checked)
                else:
                    ui.table_files.item(row, 2).setCheckState(
                        QtCore.Qt.Unchecked)

        updateBatch()

    def markPreparedAndUntracked():
        """
        Mark all files in table for processing which have trackingbox files
        and lack tracking results.
        """
        if ui.file_names is not None:
            for row in range(0, len(ui.file_names)):
                if ui.check_prepared[row] & (not ui.check_tracked[row]):
                    ui.table_files.item(row, 2).setCheckState(
                        QtCore.Qt.Checked)
                else:
                    ui.table_files.item(row, 2).setCheckState(
                        QtCore.Qt.Unchecked)

        updateBatch()

    def clearMarks():
        """
        Deselect all items in file table
        """
        if ui.file_names is not None:
            for row in range(0, len(ui.file_names)):
                ui.table_files.item(row, 2).setCheckState(QtCore.Qt.Unchecked)

        updateBatch()

    def setTrackingBox():
        print("...ui_interface.setTrackingBox...")

        ui.lock_settings.acquire()
        ui._playback = False
        ui.lock_settings.release()

        ui.draw_tracking_box = True
        ui.trackbox.start_frame = ui.current_frame
        ui.mouseBusy()
        ui.statusMessage("Click the upper left corner of the tracking box")

    def setBearing():
        ui.set_bearing = True
        ui.mouseBusy()
        ui.statusMessage("Click the starting position of individual")

    def fpsChanged(value):
        """
        Convert he user specified frames per second to frame period and save
        value.
        """
        # Calculate the frame period (invers of fps)
        ui.frame_period = 1/value

    def changeTab():
        """
        Redraw the current videoframe after changing tabs.
        """
        vn.showFrame(ui, ui.current_frame)

    # ------ Update tracking parameters ------

    def updateParamMethod():
        print("...ui_interface.updateParamMethod...")
        if ui.trackbox is not None:

            ui.lock_settings.acquire()
            ui._playback = False
            value_changed = ui.trackbox.object_detection.grabParams()
            ui.lock_settings.release()

            updateParamBw(value_changed)

    def updateParamBw(value_changed = False):
        """
        Pause playback and update object detection settings
        """
        print("...ui_interface.updateParamBw...")
        # Pause playback when updating settings to avoid segfault
        ui.lock_playback.acquire()
        ply = ui._playback
        ui._playback = False
        ui.lock_playback.release()

        ui.lock_settings.acquire()
        if ui.trackbox is not None:
            value_changed = ui.trackbox.object_detection.grabParams() or \
                value_changed
        ui.lock_settings.release()

        ui.lock_playback.acquire()
        ui._playback = ply
        ui.lock_playback.release()

        if value_changed:
            print("......Object detection parameters changed")

        updateParamMidline(value_changed)

    def updateParamMidline(redraw = True):
        """
        Update parameters for midline estimation and filtering
        """
        if ui.video is not None:
            print("...ui_interface.updateParamMidline...")
            ui.lock_settings.acquire()
            ret = ui.trackbox.setupParamsMidlineEstimation()
            ui.lock_settings.release()

            if not ui._playback and (ret or redraw):
                print("......Tracking parameters changed, redrawing frame")
                vn.showFrame(ui, ui.current_frame)

    def updateStopframe():
        """
        Update the GUI value for the stopframe.

        This is used to update the GUI text field for the stopframe after
        the user presses the button "Mark Stopframe"
        """

        try:
            ui.trackbox.stop_frame = int(ui.le_stopframe.text())
        except ValueError:
            print("cannot convert to int, defaulting stop frame to None")
            ui.trackbox.stop_frame = None

        ui.le_stopframe.setText(str(ui.trackbox.stop_frame))

        ui.statusMessage("Stopframe set to: " + str(ui.trackbox.stop_frame))

    def setStopFrame():
        """
        Set the stopframe to the current frame.
        """
        print("...ui_interface.setStopFrame...")
        ui.trackbox.stop_frame = ui.current_frame
        ui.le_stopframe.setText(str(ui.trackbox.stop_frame))

        ui.statusMessage("Stopframe set to: " + str(ui.trackbox.stop_frame))

    def savePrepData():
        """
        Save the current tracking configuration to file.

        This operates by reading the settings from the GUI then saving them as
        a dictionary list which can be pickeled.
        """
        print("...ui_interface.savePrepData...")
        if ui.video is None:
            ui.statusMessage("No video loaded, cannot save tracking settings.")
            return

        # ------------ Save user config as dictionary
        trackbox_saved = dict(start_frame=ui.trackbox.start_frame,
                              stop_frame=ui.trackbox.stop_frame,
                              width=ui.trackbox.width,
                              height=ui.trackbox.height,
                              center=ui.trackbox.center,
                              arena=ui.trackbox.arena,
                              center_offset=ui.trackbox.center_offset)

        config = dict(trackbox_attrib=trackbox_saved)

        # Object detection mode
        if ui.rb_mog2.isChecked():
            config["gb_objectDetection"] = 0
        elif ui.rb_timeOffset.isChecked():
            config["gb_objectDetection"] = 1
        elif ui.rb_grayscaleThreshold.isChecked():
            config["gb_objectDetection"] = 2

        # MOG2
        config["le_history"] = ui.le_history.text()
        config["sb_mahalanobisDistance"] = ui.sb_mahalanobisDistance.value()
        # Time offset
        config["le_referenceFrameOffset"] = ui.le_referenceFrameOffset.text()
        config["cb_preferredReferenceFrame"] = ui.cb_preferredReferenceFrame.\
            currentIndex()
        config["sb_threshold_timeOffset"] = ui.sb_threshold_timeOffset.value()
        # Grayscale Threshold
        config["sb_threshold_grayscale"] = ui.sb_threshold_grayscale.value()
        config["cb_objectOnBackground"] = ui.cb_objectOnBackground.\
            currentIndex()

        config["le_stopframe"] = ui.le_stopframe.text()

        # Binary cleaning
        config["cb_close"] = ui.cb_close.isChecked()
        config["sb_close_diameter"] = ui.sb_close_diameter.value()
        config["sb_close_iterations"] = ui.sb_close_iterations.value()
        config["cb_open"] = ui.cb_open.isChecked()
        config["sb_open_diameter"] = ui.sb_open_diameter.value()
        config["sb_open_iterations"] = ui.sb_open_iterations.value()
        config["sb_open_iterations"] = ui.sb_open_iterations.value()
        config["sb_minBlobArea"] = ui.sb_minBlobArea.value()

        # Midline estimation
        config["le_bearing"] = ui.le_bearing.text()
        config["sb_lowPassFilter"] = ui.sb_lowPassFilter.value()
        config["sb_rawPointInterval"] = ui.sb_rawPointInterval.value()
        config["sb_nTrackPoints"] = ui.sb_nTrackPoints.value()
        config["sb_order"] = ui.sb_order.value()

        with open(os.path.join(ui.currentDir, ui.current_prepared),
                  'wb') as file:
            dill.dump(config, file)

        # Update file list
        listFiles()

        ui.statusMessage("Settings saved to " + ui.current_prepared)

    def loadPrepData():
        """
        Load the current tracking configuration if file exists.
        """
        print("...ui_interface.loadPrepData...")
        if ui.video is None:
            ui.statusMessage("No video loaded, cannot load tracking settings.")
            return

        with open(os.path.join(ui.currentDir,
                               ui.current_prepared), 'rb') as file:
            config = dill.load(file)

        print("......settings to load:", config)

        blockSignals(True)  # Block signals to precent QWidget callbacks
        # Restore saved trackbox attributes
        trackbox_loaded = config["trackbox_attrib"]
        ui.trackbox.start_frame = trackbox_loaded["start_frame"]
        ui.trackbox.stop_frame = trackbox_loaded["stop_frame"]
        ui.trackbox.width = trackbox_loaded["width"]
        ui.trackbox.height = trackbox_loaded["height"]
        ui.trackbox.center = trackbox_loaded["center"]
        ui.trackbox.arena = trackbox_loaded["arena"]
        ui.trackbox.center_offset = trackbox_loaded["center_offset"]

        # Object detection mode
        if config["gb_objectDetection"] == 0:
            ui.rb_mog2.setChecked(True)
        elif config["gb_objectDetection"] == 1:
            ui.rb_timeOffset.setChecked(True)
        if config["gb_objectDetection"] == 2:
            ui.rb_grayscaleThreshold.setChecked(True)

        # MOG2
        ui.le_history.setText(config["le_history"])
        ui.sb_mahalanobisDistance.setValue(config["sb_mahalanobisDistance"])
        # Time offset
        ui.le_referenceFrameOffset.setText(config["le_referenceFrameOffset"])
        ui.cb_preferredReferenceFrame.\
            setCurrentIndex(config["cb_preferredReferenceFrame"])
        ui.sb_threshold_timeOffset.setValue(config["sb_threshold_timeOffset"])
        # Grayscale Threshold
        ui.sb_threshold_grayscale.setValue(config["sb_threshold_grayscale"])
        ui.cb_objectOnBackground.\
            setCurrentIndex(config["cb_objectOnBackground"])

        ui.le_stopframe.setText(config["le_stopframe"])

        # Binary cleaning
        ui.cb_close.setChecked(config["cb_close"])
        ui.sb_close_diameter.setValue(config["sb_close_diameter"])
        ui.sb_close_iterations.setValue(config["sb_close_iterations"])
        ui.cb_open.setChecked(config["cb_open"])
        ui.sb_open_diameter.setValue(config["sb_open_diameter"])
        ui.sb_open_iterations.setValue(config["sb_open_iterations"])
        ui.sb_minBlobArea.setValue(config["sb_minBlobArea"])

        # Midline estimation
        ui.le_bearing.setText(config["le_bearing"])
        ui.sb_lowPassFilter.setValue(config["sb_lowPassFilter"])
        ui.sb_rawPointInterval.setValue(config["sb_rawPointInterval"])
        ui.sb_nTrackPoints.setValue(config["sb_nTrackPoints"])
        ui.sb_order.setValue(config["sb_order"])

        # Update parameters
        selectTrackingAlgorithem()
        ui.statusMessage("Settings loaded from " + ui.current_prepared)

        blockSignals(False) # Reenable signals
        print("Settings loaded form disk.")

    def typeCurrentFolder():
        # Check if folder is valid
        path = ui.le_folder.text()

        # Valid path provided
        if os.path.exists(path):
            ui.currentDir = path
            listFiles()
            ui.statusMessage("......Working directory changed.")
        # Bad path provided
        else:
            ui.le_folder.setText(ui.currentDir)
            ui.statusMessage("Bad path given, working directory not changed.")

    def blockSignals(status):
        """
        Block or unblock signals form UI tacking settings elements.

        This can be used for disabling value change callbacks when loading
        ui settings from files.
        """
        ui.rb_mog2.blockSignals(status)
        ui.rb_timeOffset.blockSignals(status)
        ui.rb_grayscaleThreshold.blockSignals(status)

        # MOG2
        ui.le_history.blockSignals(status)
        ui.sb_mahalanobisDistance.blockSignals(status)
        # Time offset
        ui.le_referenceFrameOffset.blockSignals(status)
        ui.cb_preferredReferenceFrame.blockSignals(status)
        ui.sb_threshold_timeOffset.blockSignals(status)
        # Grayscale Threshold
        ui.sb_threshold_grayscale.blockSignals(status)
        ui.cb_objectOnBackground.blockSignals(status)

        ui.le_stopframe.blockSignals(status)

        # Binary cleaning
        ui.cb_close.blockSignals(status)
        ui.sb_close_diameter.blockSignals(status)
        ui.sb_close_iterations.blockSignals(status)
        ui.cb_open.blockSignals(status)
        ui.sb_open_diameter.blockSignals(status)
        ui.sb_open_iterations.blockSignals(status)
        ui.sb_minBlobArea.blockSignals(status)

        # Midline estimation
        ui.le_bearing.blockSignals(status)
        ui.sb_lowPassFilter.blockSignals(status)
        ui.sb_rawPointInterval.blockSignals(status)
        ui.sb_order.blockSignals(status)
        ui.sb_nTrackPoints.blockSignals(status)

    def trackFile():
        if ui.lock_trackButton.locked():
            return

        ui.lock_trackButton.acquire()
        if ui.track_file:

            ui.lock_settings.acquire()
            # Pause playback
            ui._playback = False

            print("Stop Tracking")

            # Disable tracking flags
            ui.track_batch = False
            ui.track_file = False
            ui.tracking_loop = False

            # Set UI to normal mode
            ui.trackMode(False)
            ui.lock_settings.release()

        else:
            track()
        ui.lock_trackButton.release()

    def trackBatch():
        if not ui.track_batch:
            ui.track_batch = True
            ui.file_batch_idx = 0

        updateBatch()

        ui.lock_settings.acquire()
        ui._playback = False
        ui.lock_settings.release()

        print("Batch:\n", ui.file_batch)


        # Load file in batch processing queue
        if len(ui.file_batch) > 0 and ui.file_batch_idx == 0:
            ui.table_files.setCurrentCell(ui.file_batch[ui.file_batch_idx], 2)

            ui.pb_reloadSetup.click()
            # Start tracking
            track()

    def track():
        if ui.trackbox is None:
            return None

        print("final points:", ui.trackbox.midline_estimation.final_points)

        # Check if file can be tracked
        if ui.trackbox.start_frame is not None and \
                ui.trackbox.midline_estimation.ready() and \
                len(ui.trackbox.midline_estimation.final_points) > 0:

            ui.trackMode(True)

            ui.track_file = True
            ui.tracking_loop = True

            # Reset file output
            ui.to_write = "frame, midlinePt, x, y\n"

            # Move to first frame and start tracking
            vn.showFrame(ui, ui.trackbox.start_frame)

            # Start playback loop
            ui.lock_playback.acquire()
            ui._playback = True
            ui.lock_playback.release()
        else:
            print("Cannot track, tracking paramters are not set")

    # ---------------------- Link UI elements ---------------------------------
    # Detection method
    ui.rb_grayscaleThreshold.clicked.connect(selectTrackingAlgorithem)
    ui.rb_timeOffset.clicked.connect(selectTrackingAlgorithem)
    ui.rb_mog2.clicked.connect(selectTrackingAlgorithem)
    # Folder Select
    ui.pb_selectFolder.clicked.connect(selectFolder)
    ui.le_filter.editingFinished.connect(listFiles)
    ui.le_folder.editingFinished.connect(typeCurrentFolder)
    # File select
    ui.table_files.currentCellChanged.connect(selectFile)
    # Marking files for batch processing
    ui.pb_markPrepared.clicked.connect(markPrepared)
    ui.pb_markPreparedAndUntracked.clicked.connect(markPreparedAndUntracked)
    ui.pb_clearMarks.clicked.connect(clearMarks)
    # Link tracking box/arena functions
    ui.pb_setTrackingBox.clicked.connect(setTrackingBox)
    ui.pb_setBearing.clicked.connect(setBearing)
    ui.pb_markStopframe.clicked.connect(setStopFrame)
    ui.le_stopframe.editingFinished.connect(updateStopframe)
    # Playback
    ui.sb_fps.valueChanged.connect(fpsChanged)
    ui.pb_playStop.clicked.connect(ui.togglePlaybackStatus)
    ui.pb_loopTracking.clicked.connect(ui.playbackTracking)
    # Update UI images
    ui.tab_widget.currentChanged.connect(changeTab)
    # ------------ Callbacks for user changing GUI params
    # MOG2
    ui.le_history.textChanged.connect(updateParamMethod)
    ui.sb_mahalanobisDistance.valueChanged.connect(updateParamMethod)
    # Time offset
    ui.le_referenceFrameOffset.editingFinished.connect(updateParamMethod)
    ui.cb_preferredReferenceFrame.currentIndexChanged.\
        connect(updateParamMethod)
    ui.sb_threshold_timeOffset.editingFinished.connect(updateParamMethod)
    # Greyscale
    ui.sb_threshold_grayscale.editingFinished.connect(updateParamMethod)
    ui.cb_objectOnBackground.currentIndexChanged.connect(updateParamMethod)
    # Stopframe
    ui.le_stopframe.textChanged.connect(updateParamMethod)
    # Binary image generation
    ui.cb_close.stateChanged.connect(updateParamBw)
    ui.sb_close_diameter.editingFinished.connect(updateParamBw)
    ui.sb_close_iterations.editingFinished.connect(updateParamBw)
    ui.cb_open.stateChanged.connect(updateParamBw)
    ui.sb_open_diameter.editingFinished.connect(updateParamBw)
    ui.sb_open_iterations.editingFinished.connect(updateParamBw)
    ui.sb_minBlobArea.editingFinished.connect(updateParamBw)
    # Midline estimation
    ui.le_bearing.editingFinished.connect(updateParamMidline)
    ui.sb_lowPassFilter.editingFinished.connect(updateParamMidline)
    ui.sb_order.editingFinished.connect(updateParamMidline)
    ui.sb_rawPointInterval.editingFinished.connect(updateParamMidline)
    ui.sb_nTrackPoints.editingFinished.connect(updateParamMidline)
    # Save/load settings
    ui.pb_saveSetup.clicked.connect(savePrepData)
    ui.pb_reloadSetup.clicked.connect(loadPrepData)
    # Tracking data
    ui.pb_trackFile.clicked.connect(trackFile)
    ui.pb_trackBatch.clicked.connect(trackBatch)
    # ------ Init visibility of UI elements
    selectTrackingAlgorithem()
