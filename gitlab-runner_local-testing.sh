#!/bin/bash

# Exit script on error
set -e

sudo gitlab-runner exec docker --docker-pull-policy=if-not-present build-ubuntu
sudo gitlab-runner exec docker --docker-pull-policy=if-not-present build-windows
sudo gitlab-runner exec docker --docker-pull-policy=if-not-present user_manual
